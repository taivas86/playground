<?php

namespace App\Controller\CodingBat\Array2;

//Given a number n, create and return a new int array of length n, containing the numbers 0,
// 1, 2, ... n-1. The given n may be 0, in which case just return a length 0 array.
// You do not need a separate if-statement for the length-0 case;
// the for-loop should naturally execute 0 times in that case, so it just works.
//fizzArray(4) → [0, 1, 2, 3]
//fizzArray(1) → [0]
//fizzArray(10) → [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

class FizzArray {
    public static function solveFizzArray($int) :array {

        for ($i=0; $i<$int; $i++) {
            $arr[]=$i;
        }

        return $arr;
    }
}

