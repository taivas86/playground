<?php

namespace App\Controller\CodeWars;

class Tribonnachi
{
    public static function solveTribonnachi(array $signature, int $n)
    {
        $res = [];
        for ($i = 0; $i < $n; $i++) {
            if ($i < 3)
                $res[$i] = $signature[$i];
            else
                $res[$i] = $res[$i-1] + $res[$i-2] + $res[$i-3];
        }
        return $res;
    }
}