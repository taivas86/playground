<?php

namespace App\Controller\TRP;

class UserInh
{
    private $name;
    private $age;
    private $birthday;

    public function __construct(string $name, string $birthday)
    {
        $this->name = $name;
        $this->birthday = $birthday;
        $this->age = $this->calculateAge($birthday);
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    public function calculateAge($birthday)
    {
        return \DateTime::createFromFormat('Y-m-d', $birthday)
            ->diff(new \DateTime('now')
            )->y;
    }
}