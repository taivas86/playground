<?php

namespace App\Controller\CodingBat\String1;



//Given 2 strings, return their concatenation, except omit the first char of each.
// The strings will be at least length 1.
//nonStart("Hello", "There") → "ellohere"
//nonStart("java", "code") → "avaode"
//nonStart("shotl", "java") → "hotlava"



class NonStart {

    public static function solveNonStart($stringA, $stringB) {

        if (strlen($stringA) <=1 || strlen($stringB) <=1) {
            return "bb";
        }

        return substr($stringA,1).substr($stringB,1);


        return "test";

    }

}