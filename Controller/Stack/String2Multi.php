<?php

namespace App\Controller\Stack;
https://stackoverflow.com/questions/49563864/how-to-convert-a-string-to-a-multidimensional-recursive-array-in-php/49563971#49563971

class String2Multi
{
    public static function solveString2Multi(string $string): array
    {
        $init = explode(',', $string);
        $pathes = [];

        foreach ($init as $chunk) {
            $parts = explode('.', $chunk);
            $path = array_pop($parts);

            foreach (array_reverse($parts) as $part) {
                $path = [$part=>$path];
            }

            $pathes[] = $path;
        }

        dump(call_user_func_array('array_merge_recursive', $pathes));
    }
}