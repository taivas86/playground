<?php

namespace App\Controller\CodingBat\Array3;

class Fix45
{
    public static function solveFix45($nums)
    {
        $len = count($nums);
        for ($i = 0; $i < $len - 1; $i++) {
            if ($nums[$i] === 4) {
                for ($j = $i; $j < $len; $j++) {
                    if ($nums[$j] == 5) {
                        if ($j > 0 && $nums[$j - 1] != 4) {
                            $tmp = $nums[$i + 1];
                            $nums[$i + 1] = 5;
                            $nums[$j] = $tmp;
                        } elseif ($j == 0) {
                            $tmp = $nums[$i + 1];
                            $nums[$nums + 1] = 5;
                            $nums[$j] = $tmp;
                        }
                    }
                }
            }
            return $nums;
        }
    }
}
