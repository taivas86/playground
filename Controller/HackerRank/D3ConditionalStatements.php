<?php

namespace App\Controller\HackerRank;

class D3ConditionalStatements
{
    public static function solveD3($n)
    {
        $isEven = ($n % 2 == 0);


        if (!$isEven) {
            $result = 'Wierd';
        }

        if ($isEven && $n >= 2 && $n <= 5) {
            $result = 'Not Wierd';
        }

        if ($isEven && $n >= 6 && $n <= 20) {
            $result = 'Wierd';
        }

        if ($isEven && $n >= 20) {
            $result = 'Not Wierd';
        }


        return $result;

    }
}