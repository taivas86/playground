<?php


namespace App\Controller\CodingBat\WarmUp1;

//Given 2 int values, return whichever value is nearest to the value 10,
//or return 0 in the event of a tie.
//Note that Math.abs(n) returns the absolute value of a number.
//close10(8, 13) → 8
//close10(13, 8) → 8
//close10(13, 7) → 0

class Close10
{
    public static function solveClose10($a, $b)
    {
        $dA = abs(10-$a);
        $dB = abs(10-$b);

        dump($dA, $dB);

        if ($dA < $dB) {
            return $a;
        }

        if ($dB < $dA) {
            return $b;
        }

        return 0;
    }
}