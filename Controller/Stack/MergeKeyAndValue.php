<?php

namespace App\Controller\Stack;

//https://stackoverflow.com/questions/37342992/php-merge-two-arrays-on-the-same-key-and-value

class MergeKeyAndValue
{
    public static function solveMergeKeyAndValue($array1, $array2)
    {
        $newArray =array();

        foreach($array1 as $key => $val)
        {
            $ids = array_map(function ($ar) {return $ar['ur_user_id'];}, $array2); //get the all the user ids from array 2
            $k = array_search($val['ur_user_id'],$ids); // find the key of user id in ids array
            $newArray[] = array_merge($array1[$key],$array2[$k]); /// merge the first array key with second
        }

        echo "<pre>"; print_r($newArray);

    }
}