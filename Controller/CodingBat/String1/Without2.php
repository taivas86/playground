<?php

namespace App\Controller\CodingBat\String1;

//Given a string, if a length 2 substring appears at both its beginning and end,
// return a string without the substring at the beginning, so "HelloHe" yields "lloHe".
// The substring may overlap with itself, so "Hi" yields "". Otherwise, return the original string unchanged.
//without2("HelloHe") → "lloHe"
//without2("HelloHi") → "HelloHi"
//without2("Hi") → ""


class Without2 {
    public static function solveWithout2($string) : string {

        $endNoodle = substr($string, -2);
        $startNoodle = substr($string, 0, 2);

        if($startNoodle == $endNoodle) {
            return substr_replace($string,"", 0, 2);
        }

        return $string;

    }
}