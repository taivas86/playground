<?php

namespace App\Controller\TRP;

class Student extends UserInh
{
    private $course;

    public function __construct($name, $age, $course)
    {
        parent::__construct($name, $age);
        $this->course = $course;
    }

    public function getCourse()
    {
        return $this->course;
    }
}