<?php

namespace App\Controller\TRP;

class Programmer extends EmployeeInh
{
    private $langs = [];

    public function setLangs($lang) {
        $this->langs[] = $lang;
    }

    public function getLangs() {
        return $this->langs;
    }


}