<?php

namespace App\Controller\CodeWars;

class CountSmiles
{
    public static function solveCountSmiles(array $arr): int
    {
        $count = 0;
        $len = count($arr);

        for ($i = 0; $i < $len; $i++) {
            $el = $arr[$i];

            if (strlen($el) == 2) {
                if (($el[0] == ":" || $el[0] == ";")
                    && ($el[1]) == ")" || $el[1] == "D") {
                    $count++;
                }
            }

            if (strlen($el) == 3) {
                if (($el[0] == ":" || $el[0] == ";")
                    && ($el[1] == "-" || $el[1] == "~")
                    && ($el[2] == ")" || $el[2] == "D")) {
                    $count++;
                }
            }
        }

        return $count;


    }
}

