<?php

namespace App\Controller\CodingBat\Array2;

//Given an array of ints, return true if
//the number of 1's is greater than the number of 4's
//more14([1, 4, 1]) → true
//more14([1, 4, 1, 4]) → false
//more14([1, 1]) → true

class More14
{

    public static function solveMore14($arr): bool
    {

        $ones = 0;
        $fours = 0;

        for ($i = 0; $i < count($arr); $i++) {

            if ($arr[$i] === 1) {
                $ones++;
            }

            if ($arr[$i] === 4) {
                $fours++;
            }
        }

        return $ones>$fours;
    }
}