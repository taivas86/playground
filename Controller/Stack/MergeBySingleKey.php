<?php

namespace App\Controller\Stack;

https://stackoverflow.com/questions/29505098/how-to-merge-combine-two-values-into-single-key-in-the-same-array

class MergeBySingleKey
{
    public static function solveMergeBySingleKey()
    {
        $array = [
            "agent_name_1" => "Agent 1",
            "agent_phone_1" => "0123456",
            "agent_company_1" => "My Company",
            "agent_email_1" => "agent@yahoo.com",
            "agent_address_1" => "United States"
        ];

        $buff = [];
        $address = '';

        foreach ($array as $key=>$value) {

            if($key != 'agent_company_1' && $key !='agent_address_1') {
                $buff[$key]=$value;
            }

            if ($key == 'agent_company_1' || $key =='agent_address_1') {
                $address .= $value.", ";
                $buff['agent_address_1']=chop($address,", ");
            }
        }

        return $buff;






    }
}