<?php


namespace App\Controller\CodingBat\Warmup2;

//Given an array of ints, return true if it contains a 2, 7, 1 pattern: a value,
//followed by the value plus 5, followed by the value minus 1.
//Additionally the 271 counts even if the "1" differs by 2 or less from the correct value.

//has271([1, 2, 7, 1]) → true
//has271([1, 2, 8, 1]) → false
//has271([2, 7, 1]) → true

class Has271
{
    public static function  solveHas271($arr) {

        for ($i=0; $i<count($arr); $i++) {
            $val = $arr[$i];

            if(($arr[$i+1] == ($val+5) && ($arr[$i+2] - ($val-1)) <= 2) ) {
                    return true;
            }
        }

        return -1;
    }
}