<?php

namespace App\Controller\CodingBat\Recursion1;

class Triangle {
    public static function solveTriangle($rows) {

        if ($rows == 0) {
            return 0;
        }

        return $rows+ self::solveTriangle($rows-1);

    }
}