<?php


namespace App\Controller\CodingBat\WarmUp1;


class Front3
{
    public static function solveFront3($string)
    {
        $newStr = substr($string, 0, 3);

        $emptyString = "";

        for ($i = 1; $i <= 3; ++$i) {
            $emptyString .= $newStr;
        }
        return $emptyString;
    }
}