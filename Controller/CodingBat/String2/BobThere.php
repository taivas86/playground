<?php

namespace App\Controller\CodingBat\String2;

//Return true if the given string contains a "bob" string, but where the middle 'o' char can be any char.
//bobThere("abcbob") → true
//bobThere("b9b") → true
//bobThere("bac") → false

class BobThere
{

    public static function solveBobThere($str): bool
    {

        for ($i = 0; $i < strlen($str); $i++) {
            $bb = substr($str, $i, $i + 3);
            if ($bb[0] == "b" && $bb[2] === "b") {
                return true;
            }
        }
        return false;
    }

}