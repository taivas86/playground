<?php

namespace App\Controller\CodingBat\Array2;

//Given an array of ints, return true if the sum of all the 2's in the array is exactly 8.
//sum28([2, 3, 2, 2, 4, 2]) → true
//sum28([2, 3, 2, 2, 4, 2, 2]) → false
//sum28([1, 2, 3, 4]) → false

class Sum28
{

    public static function solveSum28($arr): bool
    {

        $sum = 0;

        for ($i = 0; $i < count($arr); $i++) {
            if ($arr[$i] == 2) {
                $sum += $arr[$i];
            }
        }

        if ($sum !=8) {
            return false;
        }

        return true;

    }
}
