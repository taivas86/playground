<?php

namespace App\Controller\CodingBat\String1;

// Given a string, if the first or last chars are 'x', return the string without those 'x' chars,
// and otherwise return the string unchanged.
//withoutX("xHix") → "Hi"
//withoutX("xHi") → "Hi"
//withoutX("Hxix") → "Hxi"


class WithoutX {

    public static function solveWithoutX($string): string {

        if (substr($string,0,1) == "x") {
            $string = substr_replace($string, "",0, 1);
        }

        if (substr($string,-1) == "x") {
            $string = substr_replace($string, "",-1);
        }

        return $string;
    }
}
