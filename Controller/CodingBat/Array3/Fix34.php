<?php


namespace App\Controller\CodingBat\Array3;


class Fix34
{
    public static function solveFix34($nums)
    {
        $i = 0;

        while ($i < count($nums) && $nums[$i] != 3) {
            $i++;

            $j = $i + 1;

            while ($j < count($nums) && $nums[$j] != 4) {
                $j++;
            }


            while ($i < count($nums)) {
                if ($nums[$i] == 3) {
                    $temp = $nums[$i + 1];
                    $nums[$i + 1] = $nums[$j];
                    $nums[$j] = $temp;

                    while ($j < count($nums) && $nums[$j] != 4) {
                        $j++;
                    }

                }
                $i++;
            }

        }

        return $nums;
    }
}