<?php

namespace App\Controller\Stack;

//$array = [
//    2015 => '2015',
//    2016 => '2016'
//];

class OldUnset
{
    public static function solveUnset()
    {
        $array = array((int)0 => array((int)2015 => '2015'), (int)1 => array((int)2016 => '2016'), (int)2 => array((int)2017 => '2017'), (int)3 => array((int)2018 => '2018'), (int)4 => array((int)2019 => '2019'), (int)5 => array((int)2020 => '2020'), (int)6 => array((int)2021 => '2021'), (int)7 => array((int)2022 => '2022'), (int)8 => array((int)2023 => '2023'));

        $newArray = [];
        foreach ($array as $years => $year) {
            $key = key($year);
            $value = current($year);
            $newArray[$key] = $value;
        }

        dump($newArray);

    }
}