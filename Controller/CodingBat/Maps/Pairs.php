<?php

namespace App\Controller\CodingBat\Maps;

class Pairs
{
    public static function solvePairMap($map)
    {
        $keys = array_keys($map);
        $klen = count($map);

        for ($i=0; $i<$klen; $i++) {
            $key = $keys[$i];
            $fChar = substr($map[$key], 0, 1);
            $lChar = substr($map[$key],-1);
            $map[$fChar] = $lChar;
            unset($map[$key]);
        }

        asort($map);

        dump($map);
    }
}
