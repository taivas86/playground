<?php

namespace App\Controller\Stack;

//https://stackoverflow.com/questions/75233183/get-values-from-url-with-php

class ValuesFromUrl
{
    public static function solveValuesFromUrl()
    {
        $string = 'http://mydomain.nl/first/second/third/';
        $url = parse_url($string);
        $rawPath = rtrim(ltrim($url['path'],'/'),'/');
        $relPathArr = explode('/', $rawPath);
        dump($relPathArr);
    }
}