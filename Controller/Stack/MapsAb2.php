<?php

namespace App\Controller\Stack;
/**
 *
Modify and return the given map as follows: if the keys "a" and "b" are both in the map and have equal values, remove them both.
mapAB2({"a": "aaa", "b": "aaa", "c": "cake"}) → {"c": "cake"}
 */
class MapsAb2
{
    public static function solveMapsAb2(array $patHistories)
    {
        $keys = array_keys($patHistories);
        $keyLen = count($keys);
        $state = false;
        for ($i = 0; $i < $keyLen; $i++) {
            $key = $keys[$i];
            if (($key == 'a' and $keys[$i + 1] == 'b')
                && ($patHistories[$key] == ($patHistories[$keys[$i + 1]]))) {
                unset($patHistories[$key]);
                unset($patHistories[$keys[$i+1]]);
            }
        }

        dump($patHistories);
    }
}