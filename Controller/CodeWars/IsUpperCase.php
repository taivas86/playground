<?php

namespace App\Controller\CodeWars;

class IsUpperCase
{
    public static function is_uppercase(string $str): bool
    {
        $count = 0;
        $len = strlen($str);

        $i = 0;
        while ($i < $len) {
            if (ctype_lower($str[$i])) {
                $count++;
                break;
            }
            $i++;
        }

        return $count < 1;
    }
}