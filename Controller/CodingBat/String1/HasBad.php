<?php
namespace App\Controller\CodingBat\String1;


//Given a string, return true if "bad" appears starting at index 0 or 1 in the string,
// such as with "badxxx" or "xbadxx" but not "xxbadxx". The string may be any length,
// including 0. Note: use .equals() to compare 2 strings.
//hasBad("badxx") → true
//hasBad("xbadxx") → true
//hasBad("xxbadxx") → false


class HasBad {
    public static function solveHasBad($string) :bool {

        if ((substr($string, 0, 1) == "b") || (substr($string, 0, 2) == "xb")) {
            return true;
        }

        return false;

    }
}