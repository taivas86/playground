let evenArray= [
  0,  1,  2,  3,  4,  5,  6,  7,
  8,  9, 10, 11, 12, 13, 14, 15,
  16, 17, 18, 19, 20, 21, 22, 23,
  24, 24, 18, 0, 19, 21,5, 0, 0, 14
];

let arrayOut = [];
let start = false;
let count = 0;

for (let i = 0; i < evenArray.length; i++) {
  for (let j = 0; j < arrayOut.length; j++) {
    if ( evenArray[i] == arrayOut[j] ) {
      start = true;
    }
  }
  count++;
  if (count === 1 && start === false) {
    arrayOut.push(evenArray[i]);
  }
  start = false;
  count = 0;
}
console.log(arrayOut);