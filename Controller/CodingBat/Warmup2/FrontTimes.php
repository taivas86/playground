<?php


namespace App\Controller\CodingBat\Warmup2;

//Given a string and a non-negative int n, we'll say that the front of the string is the first 3 chars,
//or whatever is there if the string is less than length 3. Return n copies of the front;
//frontTimes("Chocolate", 2) → "ChoCho"
//frontTimes("Chocolate", 3) → "ChoChoCho"
//frontTimes("Abc", 3) → "AbcAbcAbc"

class FrontTimes
{
    public static function solveFrontTimes($string, $num) :string {

        $buff="";

        for ($i=0; $i<$num; $i++) {
            $buff.=substr($string,0,3);
        }


        return $buff;

    }
}