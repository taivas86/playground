<?php

namespace App\Controller\CodingBat\WarmUp1;


//Given 2 int values, return true if one is negative and one is positive.
//Except if the parameter "negative" is true, then return true only if both are negative.
//posNeg(1, -1, false) → true
//posNeg(-1, 1, false) → true
//posNeg(-4, -5, true) → true

class PosNeg
{
    public static function solvePostNeg($a, $b, $state)
    {

        if (($a > 0 && $b < 0) || ($a < 0 && $b > 0)) {
            return true;
        }

        if (($state == true) && ($a < 0 && $b < 0)) {
            return true;
        }

        return false;

    }

}