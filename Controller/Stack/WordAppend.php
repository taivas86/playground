<?php

namespace App\Controller\Stack;

class WordAppend
{
    public static function solveWordAppend($arr)
    {
        $len = count($arr);
        $i = 2;
        $buff = '';

        while ($i < $len) {
            $buff .= $arr[$i];
            $i+=$i+2;
        }

        return $buff;
    }
}