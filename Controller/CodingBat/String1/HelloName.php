<?php


namespace App\Controller\CodingBat\String1;


class HelloName
{
    public static function solveHelloName($name) {

        return "Hello {$name}";

    }
}