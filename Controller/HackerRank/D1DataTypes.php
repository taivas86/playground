<?php

/*
 * https://www.hackerrank.com/challenges/30-data-types/problem
 */


namespace App\Controller\HackerRank;

class D1DataTypes
{
    public static function solveD1(int $i, float $d, string $s)
    {
        dump($i, $d, $s);

        // Declare second integer, double, and String variables.
        $i2 = 0;
        $d2 = 0.0;
        $s2 = '';

        // Read and save an integer, double, and String to your variables.
        $i2 = 12;
        $d2 = 4.0;
        $s2 = 'is the best place to learn and practice coding!';

        // Print the sum of both integer variables on a new line.
        print ((int) $i+$i2)."<br>";

        // Print the sum of the double variables on a new line.
        print ((float) $d+$d2)."<br>";

        // Concatenate and print the String variables on a new line
        // The 's' variable above should be printed first.
        print ($s." ".$s2)."<br>";

    }
}