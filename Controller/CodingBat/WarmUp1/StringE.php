<?php


namespace App\Controller\CodingBat\WarmUp1;

//Return true if the given string contains between 1 and 3 'e' chars.
//stringE("Hello") → true
//stringE("Heelle") → true
//stringE("Heelele") → false

class StringE
{
    public static function solveStringE($string) {

        $count = 0;

        $string = strtolower($string);

        for ($i=0; $i<strlen($string); $i++) {

            if ($string[$i] == "e") {
                $count++;
            }
        }

        return ($count >=1 && $count <=3) ? true : false;


    }
}