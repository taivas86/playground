<?php

namespace App\Controller\Stack;


//https://stackoverflow.com/questions/75138505/filter-json-response-in-php
class FilterResponse
{
    public static function solveFilterResponse($data)
    {
        $data = json_decode($data, true);

        echo ("Account is valid <br/>");
        echo "Account status: ".$data['status']."<br/>";
        echo "Account modify time: ". $data['modified']."<br/>";
        echo "Account ID: ". $data["_id"]."<br/>";

    }
}