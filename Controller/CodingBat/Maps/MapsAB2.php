<?php

namespace App\Controller\CodingBat\Maps;

class MapsAB2
{
    public static function solveMapsAB3(array $maps)
    {
        $keys = array_keys($maps);
		$klen = count($keys);


		for ($i=0; $i<$klen; $i++) {
			$key = $keys[$i];
            if ($key == "b" && $maps['a'] == $maps[$key]) {
				unset ($maps['a']);
                unset($maps[$key]);
			}
		}

        dump($maps);

    }
}
