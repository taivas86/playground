<?php

namespace App\Controller\CodingBat\String1;

/*
Given 2 strings, a and b, return a new string
made of the first char of a and the last char of b, so "yo" and "java" yields "ya".
If either string is length 0, use '@' for its missing char.

lastChars("last", "chars") → "ls"
lastChars("yo", "java") → "ya"
lastChars("hi", "") → "h@"
*/

class LastChars
{
    public static function solveLastChars($str1, $str2)
    {
         if ($str1 == "") $str1 = "@";
         if ($str2 == "") $str2 = "@";

        $first = substr($str1, 0,1);
        $last = substr($str2, strlen($str2)-1);

        return $first.$last;


    }
}