<?php

namespace App\Controller\CodingBat\String2;

//Return the number of times that the string "code" appears anywhere in the given string,
//except we'll accept any letter for the 'd', so "cope" and "cooe" count.
//countCode("aaacodebbb") → 1
//countCode("codexxcode") → 2
//countCode("cozexxcope") → 2

class countCode {

    public static function solveCountCode($string) :int {
        $count = 0;

        for ($i=0; $i<strlen($string)-3; $i++) {
            if (($string[$i].$string[$i+1].$string[$i+3]) == "coe") {
                $count++;
            };
        }

        return $count;

    }

}