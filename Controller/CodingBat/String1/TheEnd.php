<?php


namespace App\Controller\CodingBat\String1;


//Given a string, return a string length 1 from its front, unless front is false, in which case return a string length 1 from its back.
//The string will be non-empty.
//theEnd("Hello", true) → "H"
//theEnd("Hello", false) → "o"
//theEnd("oh", true) → "o"


class TheEnd
{
    public static function solveTheEnd($string, $boolean) {

        $result = ($boolean) ? substr($string,0,1) : substr($string, -1);
        return $result;

    }
}