<?php

namespace App\Controller\Stack;

class DecodeJsonArray
{
    public static function solveDJSA()
    {
        //fixed json schema
        $json = '{
                    "prop1": {
                        "m_time": "2015-04-07 11:37:35",
                        "id": "30",
                        "msg": "Hai there 1"
                    },
                    "prop2": {
                        "m_time": "2015-04-07 11:37:36",
                        "id": "31",
                        "msg": "Hai there 2"
                    },
                
                    "prop3": {
                        "m_time": "2015-04-07 11:37:37",
                        "id": "32",
                        "msg": "Hai there 3"
                    }
                }';

        $decoded = json_decode($json, true);
        $keys = array_keys($decoded);

        $klen = count($keys);


        for ($i = 0; $i < $klen; $i++) {
            $key = $keys[$i];
            dump($decoded[$key]['m_time']);
        }


    }
}