<?php

namespace App\Controller\CodingBat\WarmUp1;

//Given an int n, return true if it is within 10 of 100 or 200.
// Note: Math.abs(num) computes the absolute value of a number.
//nearHundred(93) → true
//nearHundred(90) → true
//nearHundred(89) → false

class NearHundred
{
    public static function solveNearHundred($number)
    {

        return ((abs(100 - $number) <= 10) ||
            (abs(200 - $number) <= 10));

    }
}