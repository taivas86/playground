<?php


namespace App\Controller\CodingBat\WarmUp1;

//Given a string, return a string made of the first 2 chars (if present),
//however include first char only if it is 'o' and include the second only if it is 'z',
//so "ozymandias" yields "oz".
//startOz("ozymandias") → "oz"
//startOz("bzoo") → "z"
//startOz("oxx") → "o


class StartOz
{
    public static function solveStartOz($string)
    {
        if (substr($string, 0,2) == "oz") {
            return "oz";
        } elseif ($string[0] == "o" && $string[1] !="z") {
            return "o";
        } elseif ($string[0] != "o" && $string[1] ="z") {
            return "z";
        }
    }

}