<?php

namespace App\Controller\CodingBat\String1;

/*
Given a string and an int n, return a string made of the first and last n chars from the string.
The string length will be at least n.
nTwice("Hello", 2) → "Helo"
nTwice("Chocolate", 3) → "Choate"
nTwice("Chocolate", 1) → "Ce"
*/



class NTwice {
    public static function solveNTwice($string, $num) :string {
        if (strlen($string)<$num) {
            return $string;
        }
        return substr($string, 0, $num).substr($string,-$num);;
    }
}
