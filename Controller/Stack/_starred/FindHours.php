<?php

namespace App\Controller\Stack\_starred;

class FindHours
{
    public static function solveFindHours($json)
    {
        $dataArray = json_decode($json, true);
        $daySplit = [];

        foreach ($dataArray as $key => $subarray) {
            $len = count($subarray);
            for ($i = 0; $i < $len; $i++) {
                $startTime = new \DateTime($subarray[$i]['startHours']);
                $endTime = new \DateTime($subarray[$i]['endHours']);
                $interval = $endTime->diff($startTime);
                $daySplit[] = (['date' => $key, 'hours' => $interval->h]);
            }
        }

        $result = array();
        foreach ($daySplit as $element) {
            //группировка по дате
            $result[$element['date']][] = $element['hours'];
        }

        $buff = '';
        $sum = 0;

        foreach ($result as $key => $value) {
            $hours = array_sum($value);
            $buff .= "Дата: {$key}. Работник отработал: {$hours} часов"."\r\n";
            $sum += $hours;
        }

        return [$buff, $sum];



    }
}
