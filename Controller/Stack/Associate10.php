<?php

namespace App\Controller\Stack;

class Associate10
{
    public static function acc10()
    {
        $arr = array(
            "text"=>"1,1,1,1,1,1,3,4,4,4,2,2,4,3,5,5,5,5,5,5,5,5,2,4,3,3,2,3,3,3,4,3,3,3,3,3,3,3,3,4,1,4,1,1,4,2,2,2,2,3"
        );

        $data = array_chunk(explode(',', $arr['text']),10);

        foreach ($data as $datum) {
            echo(array_sum($datum)/count($datum));
        }
    }
}