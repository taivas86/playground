<?php

namespace App\Controller\CodingBat\Maps;

class Word0
{
    public static function solveWord0(array $map)
    {
        //outbox
//        $map = array_unique($map);
//        foreach ($map as $key=>$value) {
//            unset($map[$key]);
//            $map[$value] = 0;
//        }

        //pure
        $tmp = [];
        $start = false;
        $count = 0;
        $klen = count($map);

        for ($i = 0; $i < $klen; $i++) {
            for ($j = 0; $j < count($tmp); $j++) {
                if ($map[$i] == $tmp[$j]) {
                    $start = true;
                }
            }
            $count++;
            if ($count == 1 && $start == false) {
                array_push($tmp, $map[$i]);
            }
            $start = false;
            $count = 0;
        }

        foreach ($map as $key=>$value) {
            unset($map[$key]);
            $map[$value] = 0;
        }

        dump($map);
    }
}
