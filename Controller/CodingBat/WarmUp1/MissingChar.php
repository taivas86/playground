<?php


namespace App\Controller\CodingBat\WarmUp1;


class MissingChar
{
    public static function missingChar($string, $number)
    {
        for ($i = 0; $i <= strlen($string); $i++) {
            if ($i == $number) {
                return substr_replace($string, '', $i, 1);
            }
        }

        return $string;
    }
}