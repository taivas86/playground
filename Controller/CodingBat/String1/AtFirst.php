<?php
namespace App\Controller\CodingBat\String1;

//Given a string, return a string length 2 made of its first 2 chars.
// If the string length is less than 2, use '@' for the missing chars.
//atFirst("hello") → "he"
//atFirst("hi") → "hi"
//atFirst("h") → "h@"

class AtFirst {
    public static function solveAtFirst($string) :string {

        if (strlen($string)<2){
            return $string."@";
        }

        return substr($string, 0, 2);

    }
}
