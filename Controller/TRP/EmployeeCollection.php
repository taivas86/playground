<?php

namespace App\Controller\TRP;

class EmployeeCollection
{
    private $employees = [];

    public function add($employee) {
        $this->employees[] = $employee;
    }

    public function getTotalSalary() {
        $sum = 0;

        foreach ($this->employees as $employee) {
            $sum += $employee->getSalary();
        }

        return $sum;
    }

    public function getAll() {
        return $this->employees;
    }

    public function count(): int {
        return count($this->employees);
    }
}