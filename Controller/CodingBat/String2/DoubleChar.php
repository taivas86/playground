<?php

namespace App\Controller\CodingBat\String2;

//Given a string, return a string where for every char in the original, there are two chars.
//doubleChar("The") → "TThhee"
//doubleChar("AAbb") → "AAAAbbbb"
//doubleChar("Hi-There") → "HHii--TThheerree"

class DoubleChar {

    public static function solveDoubleChar($string) :string {

        $doubled = "";

        for ($i=0; $i < strlen($string); $i++) {

            $doubled.= $string[$i].$string[$i];
        }

        return $doubled;
    }

}


