<?php


namespace App\Controller\CodingBat\Warmup2;


//Given 2 strings, a and b, return the number of the positions where they contain the same
//length 2 substring. So "xxcaazz" and "xxbaaz" yields 3,
//since the "xx", "aa", and "az" substrings appear in the same place in both strings.
//stringMatch("xxcaazz", "xxbaaz") → 3
//stringMatch("abc", "abc") → 2
//stringMatch("abc", "axc") → 0


class StringMatch
{
    public static function solveStringMatch($strA, $strB) :string {

        $count = 0;

        for ($i=0; $i<strlen($strA)-1; $i++) {

            $aSub = substr($strA, $i, $i+2);
            $bSub = substr($strB, $i, $i+2);

            if ($aSub == $bSub) {
                $count++;
            }

            dump($aSub, $bSub);

            dump ($aSub == $bSub);
        }

        return $count;
    }
}