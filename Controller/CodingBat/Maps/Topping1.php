<?php

namespace App\Controller\CodingBat\Maps;

//topping1({"ice cream": "peanuts"}) → {"bread": "butter", "ice cream": "cherry"}

class Topping1
{
    public static function solveTopping1(array $maps)
    {
        $keys = array_keys($maps);
        $klen = count($keys);

        if ($klen == 0) {
            $maps['bread'] = 'butter';
            return $maps;
        }

        for ($i = 0; $i < $klen; $i++) {
            $key = $keys[$i];

            if ($key == 'ice cream') {
                $maps[$key] = 'cherry';
            }

            $maps['bread'] = 'butter';
        }

        asort($maps);
        return $maps;

    }
}