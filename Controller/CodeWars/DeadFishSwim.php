<?php

namespace App\Controller\CodeWars;

//parse("iiisdoso") == [ 8, 64 ];

class DeadFishSwim
{
    public static function parse($data)
    {
        $num = 0;
        $res = [];
        foreach (str_split($data) as $word) {
            if ($word == 'i') $num++;
            if ($word == 'd') $num--;
            if ($word == 's') $num *= $num;
            if ($word == 'o') $res[] = $num;
        }
        return $res;
    }
}