<?php

//https://stackoverflow.com/questions/13872744/php-in-array-inside-a-foreach

namespace App\Controller\Stack;

class InsideForeach
{
    public static function solveInsideForeach()
    {
        $aItem = array(1533, 2343, 2333);
        $data = ['items' => [0 => ["Item" => 139957, "OrderID" => 16025, "SizeID" => 24, "Price" => 46.00,], 1 => ["Item" => 2343, "OrderID" => 16024, "SizeID" => 24, "Price" => 100.00,], 2 => ["Item" => 1533, "OrderID" => 16025, "SizeID" => 24, "Price" => 150.00,], 3 => ["Item" => 2333, "OrderID" => 16026, "SizeID" => 24, "Price" => 200.00,],]];

        $count = 0;
        $dataNeedle = $data['items'];
        foreach ($dataNeedle as $key => $value) {
            $itemId = $value['Item'];
            if (in_array($itemId, $aItem)) {
                $count += $value['Price'];
            }
        }

        return $count;
    }
}