<?php

namespace App\Controller\CodingBat\Array2;

//Given an array length 1 or more of ints, return the difference between the largest and smallest values in the array.
//bigDiff([10, 3, 5, 6]) → 7
//bigDiff([7, 2, 10, 9]) → 8
//bigDiff([2, 10, 7, 2]) → 8


class BigDiff
{
    public static function solveBigDiff($arr): int
    {

        $len = count($arr);

        if ($len == 1) {
            return $arr[0];
        }

        for ($i = 0; $i < $len; $i++) {
            for ($j = $len - 1; $j > $i; $j--) {
                if ($arr[$j] < $arr[$j - 1]) {

                    $tmp = $arr[$j];
                    $arr[$j] = $arr[$j-1];
                    $arr[$j-1] = $tmp;

                }

            }
        }

        return $arr[$len-1] - $arr[0];


    }
}