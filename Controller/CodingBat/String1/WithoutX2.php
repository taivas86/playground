<?php


namespace App\Controller\CodingBat\String1;


class WithoutX2
{
    public static function solveWithoutX2($string) : string
    {

        for ($i=0; $i<strlen($string); $i++) {

            if ($string[0].$string[1] == "xx") {
                return substr_replace($string,"", 0,2);
            }

            if ($string[0] == "x") {
                return substr_replace($string,"", 0,1);
            }
        }

        return $string;


    }

}