<?php

namespace App\Controller\HackerRank;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class D4Person extends AbstractController
{
//    public $age;
//    public function __construct($initialAge){
//        if ($initialAge <=0) {
//            $this->age = 0;
//            echo 'Age is not valid, setting age to 0..';
//        }
//        $this->age = $initialAge;
//    }
//
//    public  function amIOld(){
//        if ($this->age < 13) {
//            echo ('You are young');
//        }
//
//        if ($this->age >= 13 && $this->age <=18) {
//            echo ('You are a teenager..');
//        }
//
//        echo ('You are old');
//
//    }
//    public  function yearPasses(){
//        $this->age = $this->age + 1;
//    }

    #[Route('d4', name: 'hello')]
    public function d4response(): Response
    {
        $user = new D4Person(25);
        dump($user);
        $result = 1;
        return self::json([
                'message' => $result,
            ]

        );
    }
}