<?php

namespace App\Controller\CodeWars;

class SharkPontoon
{
    public static function shark(int $pontoonDistance, int $sharkDistance, int $youSpeed, int $sharkSpeed, bool $dolphin): string {
        if ($dolphin) {
            $sharkSpeed = $sharkSpeed/2;
        }

        $sharkTime = $sharkDistance/$sharkSpeed;
        $swimTime = $pontoonDistance/$youSpeed;

        $jawsKillYou = $sharkTime < $swimTime;

        return $jawsKillYou ? 'Shark bait!' : 'Alive!';


    }

}