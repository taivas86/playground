<?php

namespace App\Controller\CodeWars;

class SetAlarm
{
    public static function solveSetAlarm(bool $employed, bool $vacation) :bool {
        if ($employed && !$vacation) {
            return true;
        }

        return false;
    }
}