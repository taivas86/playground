<?php

namespace App\Controller\CodeWars;

class CreatePhoneNumber
{
    public static function solveCreatePhoneNumber(array $numbersArray): array
    {
        $len = count($numbersArray);
        $startNums = '';
        $middleNums = '';
        $otherNums = '';

        for ($i = 0; $i < $len; $i++) {
            $element = $numbersArray[$i];
            if ($i < 3) {
                $startNums .= $element;
            }

            if ($i >= 3 && $i < 6) {
                $middleNums .= $element;
            }

            if ($i>=6 && $i<$len) {
                $otherNums .= $element;
            }
        }

        return "({$startNums}) {$middleNums}-{$otherNums}";

    }
}