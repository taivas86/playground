<?php


namespace App\Controller\CodingBat\Warmup2;


//Given a string, return a version where all the "x" have been removed.
// Except an "x" at the very start or end should not be removed.
//stringX("xxHixx") → "xHix"
//stringX("abxxxcd") → "abcd"
//stringX("xabxxxcdx") → "xabcdx"


class StringX
{
    public static function solveStringX($string): string
    {

        $str = strtolower($string);
        $slength = strlen($str);

        $buff = "";

        for ($i = 0; $i < strlen($str); $i++) {

           if ($str[0] == "x" && $str[$slength-1] == "x") {
               $substr = substr($str, 1, $slength-2);
               $buff = "x".str_replace('x',"", $substr)."x";
            } else {
               $buff.=str_replace("x", "", $str[$i]);
           }
        }

        return $buff;
    }
}