<?php

namespace App\Controller\CodingBat\String1;

/*
Given a string, return true if it ends in "ly".
endsLy("oddly") → true
endsLy("y") → false
endsLy("oddy") → false
*/

class EndsLy {
    public static function solveEndsLy($str) :bool{
        return (substr($str,-2) == "ly") ? true : false;
    }
}
