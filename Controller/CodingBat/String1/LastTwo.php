<?php

namespace App\Controller\CodingBat\String1;
/*
Given a string of any length, return a new string where the last 2 chars,
if present, are swapped, so "coding" yields "codign".
lastTwo("coding") → "codign"
lastTwo("cat") → "cta"
lastTwo("ab") → "ba"
*/

class LastTwo
{
    public static function solveLastTwo($string): string
    {
        $buff = ($string[strlen($string)-2]);
        $string[strlen($string)-2] = $string[strlen($string)-1];
        $string[strlen($string)-1] = $buff;
        return $string;
    }
}