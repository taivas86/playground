<?php


namespace App\Controller\CodingBat\String1;


//Given a string, return the string made of its first two chars, so the String "Hello" yields "He".
//If the string is shorter than length 2, return whatever there is, so "X" yields "X", and the empty string "" yields the empty string "".
//Note that str.length() returns the length of a string.
//firstTwo("Hello") → "He"
//firstTwo("abcdefg") → "ab"
//firstTwo("ab") → "ab"


class firstTwo
{
    public static function solveFirstTwo($string): string
    {
        if (empty($string)) return "empty";
        if (strlen($string) < 2) return $string;

        return substr($string,0,2);
    }
}