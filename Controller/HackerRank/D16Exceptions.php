<?php

namespace App\Controller\HackerRank;

class D16Exceptions
{
    public static function solveExceptions($str) {
        $state = intval($str);

        return ($state == 0) ? 'Bad string' : $state;
    }
}