<?php


namespace App\Controller\CodingBat\WarmUp1;

//Given three int values, a b c, return the largest.
//intMax(1, 2, 3) → 3
//intMax(1, 3, 2) → 3
//intMax(3, 2, 1) → 3

class intMax
{
    public static  function  solveIntMax($a,$b,$c) {

        if ($a>$b) {
            $max = $a;
        } else {
            $max = $b;
        }

        if ($c>$max) {
            $max = $c;
        }

        return $max;





    }

}