<?php

namespace App\Controller\CodingBat\Maps;

//mapAB({"a": "Hi", "b": "There"}) → {"a": "Hi", "ab": "HiThere", "b": "There"}
use Exception;

class MapsAb
{
    public static function solveMapsAb(array $arr)
    {
        $keys = array_keys($arr);
        $len = count($keys);
        $string = '';

        $isA = array_key_exists('a', $arr);
        $isB = array_key_exists('b', $arr);

        if (!$isA || !$isB) {
            throw new Exception("Array don't contain a or b key");
        }

        for ($i = 0; $i < $len; $i++) {
            $key = $keys[$i];
            if ($key == 'a' || $key == 'b') {
                $string .= $arr[$key];
            }
            $arr['ab'] = $string;
        }
        return $arr;
    }
}