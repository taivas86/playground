<?php

namespace App\Controller\Stack;
//https://stackoverflow.com/questions/75214444/calculate-percentage-of-amount-using-sql-returning-values-in-rows-instead-of-col
//php+custom db wrapper solutions

use App\DBAL\DataBaseWrapper;
use Doctrine\ORM\EntityManagerInterface;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class StocksExchange extends AbstractController
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    #[Route('privet', methods: ['GET', 'HEAD'])]
    public function get(): JsonResponse
    {
        //use wrapper for use exciting db
        $connection = $this->em->getConnection();
        if (!$connection instanceof DataBaseWrapper) {
            throw new RuntimeException('No connection');
        }
        $databaseName = 'stackoverflow';
        $connection->selectDatabase($databaseName);
        $stocksData = $connection->fetchAllAssociative('SELECT * FROM stockexchange ');

        //client split
        $clientsArr = [];
        foreach ($stocksData as $key => $item) {
            $clientsArr[intval($item['account_no'])][] = $item;
        }

        //Group data by actives
        $len = count($clientsArr);
        $buff = [];
        for ($i = 1; $i <= $len; $i++) {
            $client = $clientsArr[$i];
            for ($j = 0; $j < count($client); $j++) {
                $recordDb = $client[$j];
                $actives = $recordDb["port_percentage"] * $recordDb["positions_amt"];
                $buff[$i][$recordDb['portfilio_type']][] = $actives;
            }
        }

        for ($j = 1; $j <= count($buff); $j++) {
            $item = $buff[$j];
            foreach ($item as $key => $value) {
                $value = (count($value) > 1) ? array_sum($value) : $value[0];
                $item[$key] = $value;
            }
            $buff[$j] = $item;

            $element = $buff[$j];
            $totalAmountElement = array_sum(array_values($element));
            foreach ($element as $i => $value) {
                $element[$i] = ($value / $totalAmountElement) * 100;
            }
            $buff[$j] = $element;
        }

        return new JsonResponse($buff);
    }
}

