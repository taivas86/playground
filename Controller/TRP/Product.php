<?php

namespace App\Controller\TRP;

class Product
{
    private $name;
    private $price;
    private $quantity;

    public function __construct(string $name, float $price, float $quantity)
    {
        $this->name = $name;
        $this->price = $price;
        $this->quantity = $quantity;
    }

    public function getName() :string
    {
        return $this->name;
    }

    public function getPrice() :float
    {
        return $this->price;
    }

    public function getQuantity() : int|float
    {
        return $this->quantity;
    }
}