<?php

namespace App\Controller\Stack;

//https://stackoverflow.com/questions/75221920/extract-the-currency-rate-from-bank-api

class ExtractCurrency
{
    public static function solveExtractCurrency()
    {
        $rw = file_get_contents('/home/tosaithe/Desktop/oopbook/src/Controller/currency.json');
        $data = json_decode($rw, true);
        $exchangeRate = '';

        foreach ($data['rates'] as $value) {
            $exchangeRate = $value['mid'];
        }

        $array = ['currency'=>$data['currency'], 'exchange_rate'=>$exchangeRate];

        return $array;

    }
}