<?php

namespace App\Controller\CodingBat\String1;

//Given two strings, append them together (known as "concatenation") and return the result.
//However, if the strings are different lengths, omit chars from the longer string so it is the same length as the shorter string.
//So "Hello" and "Hi" yield "loHi". The strings may be any length.
//minCat("Hello", "Hi") → "loHi"
//minCat("Hello", "java") → "ellojava"
//minCat("java", "Hello") → "javaello"

class MinCat {
    public static function solveMinCat($string1, $string2): string {

        $str1Len = strlen($string1);
        $str2Len = strlen($string2);

        dump($str1Len, $str2Len);



        if ($str1Len > $str2Len) {
            $trim = $str1Len - $str2Len;
            return substr_replace($string1, '', 0, $trim).$string2;
        }

        if ($str2Len >$str1Len) {
            $trim = $str2Len - $str1Len;
            return $string1.substr_replace($string2, '', '', $trim);
        }

        return $string1.$string2;

    }
}