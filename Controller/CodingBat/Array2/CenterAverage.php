<?php

namespace App\Controller\CodingBat\Array2;

//Return the "centered" average of an array of ints, which we'll say is the mean average of the values,
//except ignoring the largest and smallest values in the array.
//If there are multiple copies of the smallest value, ignore just one copy,
// and likewise for the largest value. Use int division to produce the final average.
// You may assume that the array is length 3 or more.
//centeredAverage([1, 2, 3, 4, 100]) → 3
//centeredAverage([1, 1, 5, 5, 10, 8, 7]) → 5
//centeredAverage([-10, -4, -2, -4, -2, 0]) → -3


class CenterAverage {

    public static function solveCenterAverage($arr) :int {

        if (count($arr)<3) {
            return $arr;
        }

        $trimLen = count($arr)-2;
        $arr = array_splice($arr, 1, $trimLen);
        return array_sum($arr)/count($arr);
    }

}