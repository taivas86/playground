<?php


namespace App\Controller\CodingBat\Warmup2;

//Given an array of ints, return the number of times that two 6's are next to each other in the array.
//Also count instances where the second "6" is actually a 7.
//array667([6, 6, 2]) → 1
//array667([6, 6, 2, 6]) → 1
//array667([6, 7, 2, 6]) → 1



class Array667
{
    public static function solveArray667($arr) :int {

        $count = 0;

        for ($i=0; $i<count($arr)-1; $i++) {
            if($arr[$i] === 6) {
                if ($arr[$i+1] == 6 || $arr[$i] == 7) {
                    $count++;
                }
            }
        }

        return $count;
    }

}