<?php


namespace App\Controller\CodingBat\Array3;


class MaxSpan
{
    public static function solveMaxSpan($arr)
    {

        $max = 0;

        for ($i=0; $i<count($arr); $i++) {

            $j = count($arr)-1;

            while ($arr[$i] != $arr[$j]) {
                $j--;
                $span = $j-$i-1;
            }

            if ($span>$max) {
                $max = $span;
            }
        }

        return $max;
    }
}