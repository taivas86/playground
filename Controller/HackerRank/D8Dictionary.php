<?php

namespace App\Controller\HackerRank;

class D8Dictionary
{
    public static function solveD8($num, $data)
    {
        $len = count($data);
        $temp = [];
        for ($i = 0; $i < $len; $i++) {
            $new = explode(',', $data[$i]);
            if (empty ($new[1])) {
                $new = 'Not Found';
            } else {
                $new = implode("=", $new);
            }
            $temp[]=$new;
        }

        for ($i=0; $i<$num; $i++) {
            echo $temp[$i]."\n";
        }
    }
}