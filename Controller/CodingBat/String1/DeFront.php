<?php

namespace App\Controller\CodingBat\String1;

//Given a string, return a version without the first 2 chars.
//Except keep the first char if it is 'a' and keep the second char if it is 'b'.
//The string may be any length. Harder than it looks.
//deFront("Hello") → "llo"
//deFront("java") → "va"
//deFront("away") → "aay"

class DeFront {

    public static function solveDeFront($string) :string {

        for ($i=0; $i<strlen($string); $i++) {

            if ($string[1]=="b") {
                return substr_replace($string,"", 0,1);
            }

            if ($string[0]=='a' && $string[1] !=="b") {
                return substr_replace($string,"", 1,1);
            }

            if ($string[0]=='a' && $string[1] == 'b') {
                return substr_replace($string,"", 2,1);
            }

        }

        return substr_replace($string, "", 0,2);
    }
}
