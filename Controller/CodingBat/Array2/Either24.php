<?php

namespace App\Controller\CodingBat\Array2;

//Given an array of ints, return true if the array contains a 2 next to a 2 or a 4 next to a 4,
//but not both.
//either24([1, 2, 2]) → true
//either24([4, 4, 1]) → true
//either24([4, 4, 1, 2, 2]) → false

class Either24
{
    public static function solveEither24($arr) :bool
    {

        $two = 0; $four = 0;
        for ($i = 0; $i < count($arr) - 1; $i++)
        {
            if ($arr[$i] == 2 && $arr[$i + 1] == 2)
                $two++;
            if ($arr[$i] == 4 && $arr[$i + 1] == 4)
                $four++;
        }

        if ($two != 0 && $four != 0)
            return false;
        else if ($two != 0 || $four != 0)
            return true;
        else
            return false;
        }
}

