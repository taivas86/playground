<?php

namespace App\Controller\CodingBat\Array2;

//Return the number of even ints in the given array.
//Note: the % "mod" operator computes the remainder, e.g. 5 % 2 is 1.
//countEvens([2, 1, 2, 3, 4]) → 3
//countEvens([2, 2, 0]) → 3
//countEvens([1, 3, 5]) → 0

class CountEvents {

    public static function solveCountEvents($arr) :int {

        $count = 0;

        for ($i=0; $i<count($arr); $i++) {
            if ($arr[$i] % 2 == 0) {
                $count++;
            }
        }

        return $count;
    }

}