//https://stackoverflow.com/questions/75235257/sum-of-specific-field-in-one-array-if-condition-is-met

let arr1 = [{instructor_id: 7, course_id: 19, lesson_id: 1, time_spent: 0}, {
  instructor_id: 7, course_id: 19, lesson_id: 2, time_spent: 0
}, {instructor_id: 7, course_id: 19, lesson_id: 3, time_spent: 0}, {
  instructor_id: 7, course_id: 20, lesson_id: 4, time_spent: 80
}, {instructor_id: 7, course_id: 20, lesson_id: 5, time_spent: 40}, {
  instructor_id: 8, course_id: 21, lesson_id: 6, time_spent: 0
},];

let arr2 = [{id: 19, title: "Course 19", duration: 180}, {id: 20, title: "Course 20", duration: 120},];

// // expected result
// newArr = [
//   { instructor_id: 7, course_id: 19, time_spent: 0 },
//   { instructor_id: 7, course_id: 20, time_spent: 120 },
// ];
//

const result = arr2.map(({ id }) => ({
  course_id: id,
  time_spent: arr1.reduce(
    (prev, cur, index) => prev + (cur.course_id == id) * cur.time_spent
    , 0)
}));

console.log(result);