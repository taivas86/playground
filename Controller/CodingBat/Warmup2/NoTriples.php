<?php


namespace App\Controller\CodingBat\Warmup2;

//Given an array of ints, we'll say that a metarNoobie is a value appearing 3 times in a row in the array.
//Return true if the array does not contain any triples.
//noTriples([1, 1, 2, 2, 1]) → true
//noTriples([1, 1, 2, 2, 2, 1]) → false
//noTriples([1, 1, 1, 2, 2, 2, 1]) → false


class NoTriples
{
    public static function solveNoTriples($arr) {

        for ($i=0; $i<count($arr)-1; $i++) {

            if (($arr[$i]==$arr[$i+1]) && ($arr[$i+1]==$arr[$i+2])) {
                return false;
            }
        }

        return true;
    }


}