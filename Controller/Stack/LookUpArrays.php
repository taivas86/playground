<?php

namespace App\Controller\Stack;

/*https://stackoverflow.com/questions/40868637/custom-sort-an-associative-array-by-its-keys-using-a-lookup-array*/

class LookUpArrays
{
    public static function solveLookUp($aShips, $order)
    {
        //flip methods;

        dump(array_replace(array_flip($order), $aShips));

        $lookup = array_flip(array_reverse($order));

        uksort($aShips, function($a, $b) use ($lookup) {
            return ($lookup[$b] ?? -1) <=> ($lookup[$a] ?? -1);
        });

        return $aShips;

    }
}