<?php

namespace App\Controller\TRP;

class EmployeeInh extends UserInh
{
    private $salary;

    public function __construct(string $name, string $birthday, float $salary)
    {
        parent::__construct($name, $birthday);
        $this->salary = $salary;
    }

    /**
     * @return float
     */
    public function getSalary(): float
    {
        return $this->salary;
    }



}