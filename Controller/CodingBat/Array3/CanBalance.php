<?php

namespace App\Controller\CodingBat\Array3;

class CanBalance
{
    public static function solveCanBalance($nums)
    {
        $len = count($nums);
        $lsum = 0;

        for ($i = 0; $i < $len; $i++) {
            $lsum += $nums[$i];
            $rSum = 0;
            for ($j = $len - 1; $j > $i; $j--) {
                $rSum += $nums[$j];
            }

            if ($lsum == $rSum) {
                return true;
            }
        }

        return false;
    }
}
