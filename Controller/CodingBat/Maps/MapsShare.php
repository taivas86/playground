<?php

namespace App\Controller\CodingBat\Maps;

//mapShare({"a": "aaa", "b": "bbb", "c": "ccc"}) → {"a": "aaa", "b": "aaa"}

class MapsShare
{
    public static function solveMapsShare(array $arr) :array
    {
        $keys = array_keys($arr);
        $len = count($keys);
        $tmp = [];
        for ($i = 0; $i < $len; $i++) {
            $key = $keys[$i];

            if ($key === 'b') {
                $arr[$key] = $arr['a'];
            }

            if ($key === 'c') {
                unset ($arr[$key]);
            }
        }

        return $arr;
    }
}