<?php

namespace App\Controller\CodingBat\String1;

//Given a string and a second "word" string, we'll say that the word matches the string
//if it appears at the front of the string, except its first char does not need to match exactly.
//On a match, return the front of the string, or otherwise return the empty string.
//So, so with the string "hippo" the word "hi" returns "hi" and "xip" returns "hip".
//The word will be at least length 1.
//startWord("hippo", "hi") → "hi"
//startWord("hippo", "xip") → "hip"
//startWord("hippo", "i") → "h"

class StartWord
{
    public static function solveStartWord($string, $word): string
    {
        if (strlen($word)==1) {
            return $string[0];
        }

        if (substr($string, 0, 2) == $word) {
            return substr($string, 0, 2);
        }

       if (substr($string, 1, 2) && (substr($word,1,2))) {
            return $string[0].substr($string, 1, 2);
       }

        return '2';
    }
}