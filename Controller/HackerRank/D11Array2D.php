<?php

namespace App\Controller\HackerRank;

class D11Array2D
{
    public static function solveD11($arr)
    {
        for ($row = 0; $row < 4; $row++) {
            echo "<p><b>Row number $row</b></p>";
            echo "<ul>";
            for ($col = 0; $col < 3; $col++) {
                echo "<li>" . $arr[$row][$col] . "</li>";
            }
            echo "</ul>";
        }

    }
}