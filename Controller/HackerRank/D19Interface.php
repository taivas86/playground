<?php

namespace App\Controller\HackerRank;

interface AdvancedArithmetic
{
    public static function divisorSum($n);
}

class D19Interface implements AdvancedArithmetic
{
    public static function divisorSum($n)
    {
        $result = 0;
        for ($i = 1; $i <= $n; $i++) {
            if ($n % $i == 0) {
                $result += $i;
            }
        }

        return "I implemented: AdvancedArithmetic\n".$result;
    }
}