<?php

namespace App\Controller\Stack;

/*
 * Найти позицию первого и последнего вхождений
 * Найти второе и последнее слово с буквой
 */


class LastWordWithChar
{
    public static function solveLastWordChar(string $str, string $char)
    {
        $len = strlen($str);
        $occF = 0;
        $occL = 0;

        //first occurrence
        for ($i = 0; $i < $len; $i++) {
            if ($str[$i] == $char) {
                $occF = $i;
                break;
            }
        }

        //last occurrence
        for ($i = 0; $i < $len; $i++) {
            if ($str[$i] == $char) {
                $occL = $i;
            }
        }

        //first occurrence word
        $stringArray = explode(" ", $str);
        $lenStrArr =  count($stringArray);
        $buff = [];


        for ($i=0; $i<$lenStrArr; $i++) {
            $word = $stringArray[$i];
            if (str_contains($word, $char)) {
                $buff[] = $word;
            }
        }

        $firstWordOcc = $buff[1];
        $lastWordOcc = $buff[count($buff)-1];



        return ['firstOccurence' => $occF, 'lastOccurence' => $occL,
            'firstWord'=>$firstWordOcc, 'lastWord'=>$lastWordOcc];
    }
}