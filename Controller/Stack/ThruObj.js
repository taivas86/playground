//https://stackoverflow.com/questions/75151594/javascript-iterating-thru-object-array

'use strict';

const ABC = {
  video: [{id: 1, title: 'video', component: 'Video />'},],
  condensed: [{id: 2, title: 'condensed', component: '<Condensed />'},],
  full: [{id: 3, title: 'full', component: '<Full />'},],
};

const iterateObj = (obj) => {
  for (let item in obj) {
    obj[item].forEach(function ({id, title}) {
      console.log(id, title);
    });
  }
}


console.log(iterateObj(ABC));