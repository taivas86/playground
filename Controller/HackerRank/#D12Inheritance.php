<?php
//
//namespace App\Controller\HackerRank;
//
//
//use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\Routing\Annotation\Route;
//
//class D12Inheritance extends AbstractController
//{
//    #[Route('d12', name: 'd12solve')]
//    public static function D12solve(): Response
//    {
//        $student = new Student('Al','Gol',2222,100);
//        return self::json(['message' => 'hello']);
//    }
//}
//
//
//class Person
//{
//    protected $firstName;
//    protected $lastName;
//    protected $idNumber;
//    protected $scores;
//}
//
//class Student extends Person
//{
//
//    public function __construct($firstName, $lastName, $idNumber, $scores)
//    {
//        $this->firstName = $firstName;
//        $this->lastName = $lastName;
//        $this->idNumber = $idNumber;
//        $this->scores = $scores;
//    }
//
//    public function getFirstName()
//    {
//        return $this->firstName;
//    }
//
//    public function getLastName()
//    {
//        return $this->lastName;
//    }
//
//    public function getId()
//    {
//        return $this->idNumber;
//    }
//
//    public function getRank($range)
//    {
//
//        if ($range>count($this->scores)) {
//            return  ("Invalid range");
//        }
//
//        $arr = $this->scores;
//        $sum = 0;
//        for ($i = 0; $i < $range; $i++) {
//            $sum += $arr[$i];
//        }
//
//        $scoresAvg = $sum/$range;
//        if ($scoresAvg >= 90 && $scoresAvg <= 100) return 'O';
//        if ($scoresAvg >= 80 && $scoresAvg < 90) return 'E';
//        if ($scoresAvg >= 70 && $scoresAvg < 80) return 'A';
//        if ($scoresAvg >= 55 && $scoresAvg < 70) return 'P';
//        if ($scoresAvg >= 40 && $scoresAvg <= 55) return 'D';
//        if ($scoresAvg <= 40) return 'T';
//    }
//}
//
