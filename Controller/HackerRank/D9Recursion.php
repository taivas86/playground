<?php

namespace App\Controller\HackerRank;

class D9Recursion
{
    public static function solveD9($n)
    {
        if ($n==1){
            return 1;
        }

        return self::solveD9($n-1)*$n;
    }
}