<?php


namespace App\Controller\CodingBat\Array2;

//Given an array of ints, return true if there is a 1 in the array with a 2 somewhere later in the array.
//has12([1, 3, 2]) → true
//has12([3, 1, 2]) → true
//has12([3, 1, 4, 5, 2]) → true



class Has12
{
    public static function solveHas12($arr) :bool {

        $keyOne = 0;
        $keyTwo = 0;

        for ($i = 0; $i < count($arr); $i++) {

            $keyOne = (int)($arr[$i] == 1) ? $i : false;
            $keyTwo = (int)($arr[$i] == 2) ? $i : false;
        }

        if ($keyOne == false || $keyTwo == false) {
            return false;
        } elseif ($keyOne > $keyTwo) {
            return false;
        }

        return true;
    }
}