<?php

namespace App\Controller\Stack;

class FirstChar
{
    public static function solveFirstChar(array $strings)
    {
        $buff = [];
        foreach ($strings as $key => $value) {
            $char = substr($value, 0, 1);
            $buff[$char][] = $value;
        }
    }
}