<?php
namespace App\Controller\CodingBat\String1;

/*
Given a string and an index, return a string length 2 starting at the given index.
If the index is too big or too small to define a string length 2, use the first 2 chars.
The string length will be at least 2.
twoChar("java", 0) → "ja"
twoChar("java", 2) → "va"
twoChar("java", 3) → "ja"
 */

class TwoChar {
    public static function TwoChar($string, $num) :string {

        if (strlen($string) < 2) {
            return $string;
        }

        if($num>2) {
            $num = 0;
        }

        return substr($string, $num, 2);


        return '123';
    }
}

