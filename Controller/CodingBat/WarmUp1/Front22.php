<?php


namespace App\Controller\CodingBat\WarmUp1;

//Given a string, take the first 2 chars and return the string with the 2 chars added at both the front and back,
//so "kitten" yields"kikittenki". If the string length is less than 2, use whatever chars are there.
//front22("kitten") → "kikittenki"
//front22("Ha") → "HaHaHa"
//front22("abc") → "ababcab"

class Front22
{
    public static function solveFront22($string) {
        $firstChars = substr($string, 0, 2);
        return $firstChars.$string.$firstChars;
    }

}