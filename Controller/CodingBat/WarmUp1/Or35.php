<?php


namespace App\Controller\CodingBat\WarmUp1;

//Return true if the given non-negative number is a multiple of 3 or a multiple of 5. Use the % "mod" operator
//or35(3) → true
//or35(10) → true
//or35(8) → false


class Or35
{
    public static function solveOr35($number) {
        $result = ($number%3==0 ||$number%5==0) ? true : false;

        return $result;



    }


}