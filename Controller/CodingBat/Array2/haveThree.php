<?php

namespace App\Controller\CodingBat\Array2;

// Given an array of ints, return true if the value 3 appears in the array exactly 3 times,
// and no 3's are next to each other.
// haveThree([3, 1, 3, 1, 3]) → true
// haveThree([3, 1, 3, 3]) → false
// haveThree([3, 4, 3, 3, 4]) → false



class haveThree {

    public static function solveHaveThree($arr)  {
        $count = 0;

        for($i = 0; $i < count($arr)-1; $i++) {
            if($arr[$i] == 3 && $arr[$i+1] == 3) {
                dump($arr[$i]);
                return false;
            }
        }

        return true;
    }
}
