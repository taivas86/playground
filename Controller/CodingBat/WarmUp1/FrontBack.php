<?php

namespace App\Controller\CodingBat\WarmUp1;

//Given a string, return a new string where the first and last chars have been exchanged.
//frontBack("code") → "eodc"
//frontBack("a") → "a"
//frontBack("ab") → "ba"

class FrontBack
{
    public static function solveFrontBack($string)
    {
        if (strlen($string) < 2) {
            return $string;
        }

        for ($i = 0; $i <= strlen($string); $i++) {

            $firstChar = $string[0];
            $lastChar = $string[strlen($string)-1];

            $string[0] = $lastChar;
            $string[strlen($string)-1] = $firstChar;
        }

        return $string;

    }
}