<?php


namespace App\Controller\CodingBat\Array2;

//Given an array of ints, return true if the array contains two 7's next to each other, or there are two 7's separated by one element,
//such as with {7, 1, 7}.
//has77([1, 7, 7]) → true
//has77([1, 7, 1, 7]) → true
//has77([1, 7, 1, 1, 7]) → false


class Has77
{

    public static function solveHas77($arr) :bool {

        for ($i=0; $i<count($arr)-1; $i++) {

            if (($arr[$i]==7 && $arr[$i+1] ==7)) {
                return true;
            }

            if (($arr[$i]==7 && $arr[$i+2] ==7)) {
                return true;
            }



        }

        return false;
    }

}