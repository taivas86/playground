<?php

namespace App\Controller\TRP;

class Arr
{
    private $nums = [];
    private $sumHelper;
    private $avgHelper;

    public function __construct()
    {
        $this->sumHelper = new SumHelper();
        $this->avgHelper = new AvgHelper();
    }


    public function getSum23()
    {
        return $this->sumHelper->getSum2($this->nums) + $this->sumHelper->getSum3($this->nums);
    }

    public function avg() {
        return $this->avgHelper->getAvg($this->nums);
    }

    public function getAvgMeanSum() {
        return $this->avg() + $this->avgHelper->getMeanSquare($this->nums);
    }


    public function add($number)
    {
        $this->nums[] = $number;
    }
}
