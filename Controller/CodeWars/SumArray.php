<?php

namespace App\Controller\CodeWars;

class SumArray
{
    public static function solveSumArray(array $array)
    {
        $len = count($array);
        $max = 0;
        $min = 0;
        $tmp = 0;

        for ($i = 0; $i < $len; $i++) {
            $elt = $array[$i];
            //pure max
            if ($i == 0) {
                $max = $tmp = $elt;
            } elseif ($i > 0) {
                if ($array[$i] > $tmp) {
                    $max = $elt;
                }
            }
            //pure min
            if ($i == 0) {
                $min = $tmp = $elt;
            } elseif ($i > 0) {
                if ($elt < $tmp) {
                    $min = $elt;
                }
            }
        }

        //get sum
        $sum = 0;
        $j = 0;
        while ($j < $len) {
            if ($array[$j] != $min && $array[$j] != $max) {
                $sum += $array[$j];
            }
            $j++;
        }

        return $sum;

    }
}