<?php


namespace App\Controller\CodingBat\Recursion1;


class Fibbonaci
{
    public static function solveFibonacci($n)
    {
        if ($n <= 1) {
            return $n;
        } else {
            return self::solveFibonacci($n - 1) + self::solveFibonacci($n - 2);
        }


    }


}