<?php


namespace App\Controller\CodingBat\WarmUp1;

//Given 2 ints, a and b, return true if one if them is 10 or if their sum is 10.
//makes10(9, 10) → true
//makes10(9, 9) → false
//makes10(1, 9) → true


class Makes10
{
    public static function solveMakes10($a, $b)
    {
        if ($a == 10 || $b == 10) {
            return true;
        }

        if ($a+$b == 10) {
            return true;
        }

        return false;
    }

}