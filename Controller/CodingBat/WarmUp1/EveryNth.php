<?php


namespace App\Controller\CodingBat\WarmUp1;


//Given a non-empty string and an int N, return the string made starting with char 0,
//and then every Nth char of the string. So if N is 3, use char 0, 3, 6, ... and so on.
// N is 1 or more.w
//everyNth("Miracle", 2) → "Mrce"
//everyNth("abcdefg", 2) → "aceg"
//everyNth("abcdefg", 3) → "adg"



class EveryNth
{
    public static function solveEveryNth($string, $num) {

        $result = "";

        for ($i=0; $i<strlen($string); $i+=$num) {
           $result = $result.$string[$i];
        }

        return $result;

    }
}
