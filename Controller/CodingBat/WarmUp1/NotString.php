<?php


namespace App\Controller\CodingBat\WarmUp1;

//Given a string, return a new string where "not " has been added to the front.
//However, if the string already begins with "not", return the string unchanged.
//Note: use .equals() to compare 2 strings.
//notString("candy") → "not candy"
//notString("x") → "not x"
//notString("not bad") → "not bad"

class NotString
{
    public static function solveNotString($string) {

         if (substr($string,0,3) !== "not") {
             return "not ".$string;
         }

         return $string;

    }
}