<?php


namespace App\Controller\CodingBat\WarmUp1;

//Return true if the given string begins with "mix", except the 'm' can be anything, so "pix", "9ix" .. all count.
//mixStart("mix snacks") → true
//mixStart("pix snacks") → true
//mixStart("piz snacks") → false

class mixStart
{
    public static function solveMixStart($string)
    {
        $chunk = substr($string,1,2);

        if (strpos($string, $chunk) == true) {
            return true;
        }

        return false;

    }

}