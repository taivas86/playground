<?php

namespace App\Controller\CodingBat\String2;

//We'll say that a String is xy-balanced if for all the 'x' chars in the string,
// there exists a 'y' char somewhere later in the string.
// So "xxy" is balanced, but "xyx" is not. One 'y' can balance multiple 'x's.
//Return true if the given string is xy-balanced.
//xyBalance("aaxbby") → true
//xyBalance("aaxbb") → false
//xyBalance("yaaxbb") → false


class XYBalance
{

    public static function solveXYBalance($string): bool
    {

        $y =  false;

        for ($i = strlen($string)-1; $i>=0; $i--) {


            if ($string[$i] == "y") {
                return true;
            }

            if ($string[$i] == "x" && $y==false) {
                return false;
            }

        }



        return false;

    }

}