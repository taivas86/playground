<?php

namespace App\Controller\CodeWars;

class SuzukiClimb
{
    public static function solveSuzukiClimb($arr)
    {
        $len = count($arr);
        $sum = 0;
        for ($i=0; $i<$len; $i++) {
            $sum += array_sum($arr[$i]);
        }

        return $sum * 20;
    }
}