<?php

namespace App\Controller\CodingBat\String2;

//Given two strings, return true if either of the strings appears at the very end of the other string,
// ignoring upper/lower case differences (in other words, the computation should not be "case sensitive").
//Note: str.toLowerCase() returns the lowercase version of a string.
//endOther("Hiabc", "abc") → true
//endOther("AbC", "HiaBc") → true
//endOther("abc", "abXabc") → true


class EndOther {

    public static function solveEndOther($strA, $strB) :bool {

        $strAL = strtolower($strA);
        $strBL = strtolower($strB);
        //return substr($strAL,-3) == substr($strBL,-3);

        $str = "";

        for ($i=0; $i<strlen($strBL); $i++) {
            $pat = substr($strBL, $i, $i+3);
            if ($pat == substr($strAL, -3)) {
                return true;
            }
        }

      return false;
    }
}