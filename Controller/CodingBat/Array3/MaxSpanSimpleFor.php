<?php

namespace App\Controller\CodingBat\Array3;

class MaxSpanSimpleFor
{
    public static function solveMaxSpan($nums) {
       $len = count($nums);
       $tmp = 0;
       $max = 0;
       for($i=0; $i<=$len; $i++) {
           for ($j=$len-1; $j>$i; $j--) {
               if ($nums[$i] == $nums[$j]) {
                   $tmp = $j-$i;
                   if ($max<$tmp) {
                       $max= $tmp;
                   }
               }
           }
       }

       return $max+1;
    }
}
