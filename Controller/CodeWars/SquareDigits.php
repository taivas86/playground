<?php

namespace App\Controller\CodeWars;

class SquareDigits
{
    public static function solveSquareDigits(int $num): int
    {
        $arr = str_split($num);
        $str = '';
        $i = 0;
        while ($i < count($arr)) {
            $str.=pow($arr[$i], 2);
            $i++;
        }
        return (int) $str;
    }
}