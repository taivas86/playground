<?php

namespace App\Controller\Stack;

class FindTheDuplicate
{
    public static function solveFindTheDuplicate($array)
    {

        $dup = array_count_values(array_column($array, 'lid'));
        dump($dup);
        $store = [];

        foreach ($dup as $key => $value) {
            if ($value > 1) {
                $store[] = $key;
            }
        }

        $dumplicated = [];

        $len = count($array);
        for ($i = 0; $i < $len; $i++) {
            $subarr = $array[$i];
            if (array_intersect($subarr, $store)) {
                $dumplicated[]=$subarr;
            }
        }

        return $dumplicated;
    }
}