<?php

namespace App\Controller\CodeWars;

class PersistentBugger
{
    public static function solvePersistentBurger(int $num): int
    {
        $i=0;
        while($num > 1) {
            $num = array_product(str_split($num));
            ++$i;
        }
        return $i;
    }
}