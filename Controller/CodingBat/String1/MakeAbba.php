<?php


namespace App\Controller\CodingBat\String1;

//Given two strings, a and b, return the result of putting
//them together in the order abba, e.g. "Hi" and "Bye" returns "HiByeByeHi".
//makeAbba("Hi", "Bye") → "HiByeByeHi"
//makeAbba("Yo", "Alice") → "YoAliceAliceYo"
//makeAbba("What", "Up") → "WhatUpUpWhat"

class MakeAbba
{
    public static function solveMakeAbba($strA, $strB) : string {
        return $strA.$strB.$strB.$strA;
    }
}