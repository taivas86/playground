<?php

namespace App\Controller\CodeWars;

class BouncingBalls
{
    public static function solveBouncingBall(float $h, float $bounce, float $window)
    {
        if ($h > 0 && ($bounce > 0 && $bounce < 1) && $window < $h) {

            $current = $h*$bounce;
            $count =1;
            while($current > $window) {
                $current *= $bounce;
                $count +=2;
            }

            return $count;
        }


        return -1;
    }
}