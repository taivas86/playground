<?php

namespace App\Controller\CodingBat\Maps;

class MapBully
{
    public static function solveMapBully(array $map)
    {
        $keys = array_keys($map);
        $len = count($keys);
        $tmp = '';
        $state = array_key_exists('a', $map);
        for ($i = 0; $i < $len; $i++) {
            $key = $keys[$i];
            if ($state && $key == 'b' ) {
                $tmp = $map['a'];
                $map['a'] = '';
                $map['b'] = $tmp;
             }
        }
        return $map;
    }
}