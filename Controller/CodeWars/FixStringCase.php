<?php

namespace App\Controller\CodeWars;

class FixStringCase
{
    public static function solveFixStringCase(string $s): string
    {
        $len = strlen($s);
        $countUp = 0;
        $countLw = 0;

        for ($i = 0; $i < $len; $i++) {
            $char = $s[$i];
            !ctype_lower($char) ? $countUp++ : $countLw++;
        }

        for ($j = 0; $j < $len; $j++) {
            $char = $s[$j];

            if ($countLw > $countUp) {
                if (!ctype_lower($char)) {
                    $s[$j] = strtolower($char);
                }
            }

            if ($countLw < $countUp) {
                if (ctype_lower($char)) {
                    $s[$j] = strtoupper($char);
                }
            }

            if ($countUp == $countLw) {
                $s[$j] = strtolower($char);
            }
        }

        return $s;
    }
}

