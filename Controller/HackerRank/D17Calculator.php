<?php

namespace App\Controller\HackerRank;

class D17Calculator
{
    protected $n;
    protected $p;

    public function __construct($n, $p) {
        $this->n = $n;
        $this->p = $p;
    }

    public function power() {
        if ($this->n < 0 || $this->p < 0) {
            return 'n and p should be non-negative';
        }

        return pow($this->n, $this->p);
    }
}