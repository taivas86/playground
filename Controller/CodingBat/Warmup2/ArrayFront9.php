<?php


namespace App\Controller\CodingBat\Warmup2;

//Given an array of ints, return true if one of the first 4 elements in the array is a 9.
//The array length may be less than 4.
//arrayFront9([1, 2, 9, 3, 4]) → true
//arrayFront9([1, 2, 3, 4, 9]) → false
//arrayFront9([1, 2, 3, 4, 5]) → false

class ArrayFront9
{
    public static function solveArrayFront9($arr): bool
    {
        for ($i = 0; $i < 3; $i++) {

            for ($j = 0; $j < $arr[$i]; $j++) {
                if ($arr[$j] == 9) {
                    return true;
                }
            }
        }

        return false;
    }

}