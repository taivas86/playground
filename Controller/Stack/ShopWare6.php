<?php

namespace App\Controller\Stack;

class ShopWare6
{
    public static function solveShopWare6()
    {
        $arr = [["productNumber" => "Test1", "prices" => ["ruleId" => "cfda4784f29b40428c881e3a1878ad69", "quantityStart" => 1, "price" => [["currencyId" => "b7d2554b0ce847cd82f3ac9bd1c0dfca", "gross" => 2.0, "net" => 1.0, "linked" => true,]]], //This is the product ID
            "id" => "dc00dd078e4949bc9aadb14e62cf6924"], ["productNumber" => "Test2", "prices" => ["ruleId" => "cfda4784f29b40428c881e3a1878ad69", "quantityStart" => 1, "price" => [["currencyId" => "b7d2554b0ce847cd82f3ac9bd1c0dfca", "gross" => 2.0, "net" => 1.0, "linked" => true,]]], //This is the product ID
            "id" => "2324be8b90a54524914b86bc8f0521e4"]];

        dump($arr);
    }
}