<?php

namespace App\Controller\Stack\_starred;

//https://stackoverflow.com/questions/52184042/how-to-merge-subarrays-by-one-key-value-and-push-qualifying-values-into-a-deeper

class ImageArray
{
    public static function solveImageArray()
    {
        $images_arr = [['hash' => '948e980ed2a36d917f1b3026b04e0016', 'filename' => ['73315_73316_73317_LAKE13437_322.tif'], 'filepath' => ['_TEST_DATA']], ['hash' => '948e980ed2a36d917f1b3026b04e0016', 'filename' => ['73315_73316_73317_LAKE13437_322(1).tif'], 'filepath' => ['_TEST_DATA/subdirectory']]];

        $mergeArrayWithHash = [];
        foreach ($images_arr as $value) {
            $mergeArrayWithHash[$value['hash']]['hash'] = $value['hash'];
            $mergeArrayWithHash[$value['hash']]['filename'][] = $value['filename'][0];
            $mergeArrayWithHash[$value['hash']]['filepath'][] = $value['filepath'][0];
        }

        dump($mergeArrayWithHash);

        $result = array_values($mergeArrayWithHash);

        echo "<pre>";
        print_r($result);
        exit;
    }
}