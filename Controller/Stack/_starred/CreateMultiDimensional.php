<?php

namespace App\Controller\Stack\_starred;

//https://stackoverflow.com/questions/2579305/how-to-merge-multiple-url-path-into-multidimensional-array

class CreateMultiDimensional
{
    public static function solveArray($pathStrings)
    {
        $paths = [];

        foreach ($pathStrings as $pathString) {
            $pathParts = explode("\\", $pathString);
            $path = array_pop($pathParts);

            foreach (array_reverse($pathParts) as $pathPart) {
              $path = [$pathPart=>$path];
            }

            $paths[] = $path;
        }

        $tree = call_user_func_array('array_merge_recursive', $paths);

        return $tree;


    }
}