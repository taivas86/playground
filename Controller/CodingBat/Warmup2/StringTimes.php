<?php


namespace App\Controller\CodingBat\Warmup2;

//Given a string and a non-negative int n, return a larger string that is n copies of the original string.
//stringTimes("Hi", 2) → "HiHi"
//stringTimes("Hi", 3) → "HiHiHi"
//stringTimes("Hi", 1) → "Hi"


class StringTimes
{
    public static function solveStringTimes($string, $num) {

        $buff = "";

        for ($i=0; $i<$num; $i++) {
            $buff.=$string;
        }


        return $buff;
    }
}