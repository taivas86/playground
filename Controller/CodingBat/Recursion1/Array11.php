<?php


namespace App\Controller\CodingBat\Recursion1;


class Array11
{
    public static function solveArray11($nums, $index) {
        // exit condition;
        if ($index >= count($nums)) {
            return 0;
        }

        if ($nums[$index] == 11) {
            return 1 + self::solveArray11($nums, $index+1);
        }

        return self::solveArray11($nums, $index+1);
    }
}