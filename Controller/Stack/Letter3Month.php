<?php

namespace App\Controller\Stack;

class Letter3Month
{
    //https://stackoverflow.com/questions/75137612/replace-3-letter-month-with-its-month-number-in-an-array-of-strings
    public static function solveLetter3Month($arr)
    {
//        $dateObj = \DateTime::createFromFormat('F', 'jun');
//
//
//        dump($month);

        $len = count($arr);

        for ($i=0; $i<$len; $i++) {
            $element = $arr[$i];
            $letterMonth = substr($element, 0, 3);
            $dateObj = \DateTime::createFromFormat('F', $letterMonth);
            $month = $dateObj->format('m');
            $arr[$i] = str_replace($letterMonth, $month, $element );
        }

        return $arr;
    }
}