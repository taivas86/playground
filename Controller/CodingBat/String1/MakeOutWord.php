<?php


namespace App\Controller\CodingBat\String1;


//Given an "out" string length 4, such as "<<>>", and a word, return a new string where the word is in the middle of the out string, e.g. "<<word>>".
//Note: use str.substring(i, j) to extract the String starting at index i and going up to but not including index j.
//makeOutWord("<<>>", "Yay") → "<<Yay>>"
//makeOutWord("<<>>", "WooHoo") → "<<WooHoo>>"
//makeOutWord("[[]]", "word") → "[[word]]"

class MakeOutWord
{
    public static function solveMakeOutWord($out, $word) {
        return substr($out,0,1).$word.substr($out,2,1);
    }
}