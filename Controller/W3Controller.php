<?php

namespace App\Controller;

use App\Controller\MotorCheck\Class20Student;
use App\Controller\MotorCheck\Class21Student;
use App\Controller\MotorCheck\Class22Student;
use App\Controller\MotorCheck\Class22TEmployee;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class W3Controller extends AbstractController
{
    /**
     * @Route("hello", name="w3resources")
     */
    public function solveW3(): Response
    {
        $employee = new Class22TEmployee('abc', 'dfc', '1976-10-22', 4000);

        $res = -1;
        return self::json([
            'message' => $res,
        ]);
    }
}




