<?php


namespace App\Controller\CodingBat\Recursion1;


class Array220
{
    public static function solveArray220($nums, $index) {

        $result = 0;

        if ($index >= count($nums)) {
            return 0;
        }

        if ($nums[$index] == $nums[$index+1]/10) {
            $result = 1+self::solveArray220($nums, $index+1);
        }

        return $result > 1;


    }
}