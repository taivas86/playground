<?php


namespace App\Controller\CodingBat\Recursion1;


class Factorial
{
    public static function solveFactorial($n) {

        if ($n <= 1) {
            return 1;
        } else {
            return $n * self::solveFactorial($n-1);
        }
    }
}