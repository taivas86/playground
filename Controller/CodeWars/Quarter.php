<?php

namespace App\Controller\CodeWars;

class Quarter
{
    public static function solveQuarter(string $month) :int {
        return ceil($month/3);
    }
 }