<?php

namespace App\Controller\HackerRank;

class D20Sorting
{
    public static function solveD20($round, $nums)
    {
        $len = count($nums);
        $swap = 0;

        for ($i = 0; $i < $len; $i++) {
            for ($j = $len - 1;$j > $i ; $j--) {
                if ($nums[$j]<$nums[$j-1]) {
                    $tmp = $nums[$j];
                    $nums[$j] = $nums[$j-1];
                    $nums[$j-1] = $tmp;
                }
            }
            $swap++;
        }


        return $swap;
    }
}