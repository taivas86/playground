<?php

namespace App\Controller\CodingBat\String1;

//Given a string, return a version without the first and last char, so "Hello" yields "ell".
//The string length will be at least 2.
//withoutEnd("Hello") → "ell"
//withoutEnd("java") → "av"
//withoutEnd("coding") → "odin"


class WithoutEnd {

    public static function solveWithoutEnd($string) {

        $string = substr($string, 1);
        $string = substr($string, 0, -1);

        return $string;

    }

}
