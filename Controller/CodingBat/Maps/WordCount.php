<?php

namespace App\Controller\CodingBat\Maps;

class WordCount

{
    public static function solveWordCount($map)
    {
        //outbox
        $sums = array_count_values($map);
        $tmp = [];

        //vanilla
        foreach ($map as $item) {
            $tmp[$item] ??= 0;
            $tmp[$item] += 1;
        }

        dump($tmp);
    }
}
