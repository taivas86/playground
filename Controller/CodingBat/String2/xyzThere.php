<?php

namespace App\Controller\CodingBat\String2;

//Return true if the given string contains an appearance of "xyz" where the xyz is not directly
//preceeded by a period (.). So "xxyz" counts but "x.xyz" does not.
//xyzThere("abcxyz") → true
//xyzThere("abc.xyz") → false
//xyzThere("xyz.abc") → true

class xyzThere {

    public static function solveXyzThere($string) :bool {

        if (strlen($string) == 3 && $string=="xyz") {
            return true;
        }

        for ($i=0; $i<strlen($string); $i++) {
            if ($string[$i-1] != "." && substr($string, $i, $i+3) == "xyz" ) {
                return true;
            }
        }

        return false;
    }

}