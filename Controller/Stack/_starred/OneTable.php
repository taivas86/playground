<?php

namespace App\Controller\Stack\_starred;

//https://stackoverflow.com/questions/75368792/how-can-i-display-or-merge-selected-product-in-1-table
use SQLite3;

class OneTable
{
    public static function solveOneTable()
    {
        //data from front
        $checkedIds = [19, 22, 48];
        //our test db
        $db = new SQLite3('./src/Controller/_files/stackExample');

        //getbyone
        $tmp = [];
        $clen = count($checkedIds);
        for ($i = 0; $i < $clen; $i++) {
            $id = $checkedIds[$i];
            $res = $db->query("select * from onetable where product_id={$id}");
            while ($row = $res->fetchArray(SQLITE3_ASSOC)) {
                $tmp[] = $row;
            }
        }
        $db->close();


        $tmplen = count($tmp);
        //initTable
        $output = '';
        $output .= "<table>";

        //header
        $output .= "<tr>";
        foreach ($tmp[0] as $key => $value) {
            $output .= "<th>{$key}</th>";
        }
        $output .= "</tr>";

        //data map
        for ($j = 0; $j < $tmplen; $j++) {
            $chunk = $tmp[$j];
            $output .= '<tr>';
            foreach ($chunk as $item) {
                $output .= "<td>{$item}</td>";
            }
            $output .= "</tr>";
        }
        $output .= "</table>";

        return $output;
    }
}


