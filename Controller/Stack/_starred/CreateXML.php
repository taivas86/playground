<?php

namespace App\Controller\Stack\_starred;

//https://stackoverflow.com/questions/75145642/how-to-read-function-php-and-create-a-file-xml

class CreateXML
{
    public static function defineCustomTemplate()
    {
        $Nmfile = 302444;
        $Nmoperation = "CPTEVALITHAB";
        $Nmimport = rand(0,6);

        $customTemplate = array();
        $customTemplate[] = array('column' => 'nmfield01', 'term' => 103571, 'dataType' => 2, 'precision' => 50, 'description' => 213795, 'required' => 1,);
        $customTemplate[] = array('column' => 'nmfield02', 'term' => 102213, 'dataType' => 2, 'precision' => 50, 'description' => 302440, 'required' => 1);
        $customTemplate[] = array('column' => 'nmfield03', 'term' => 302442, 'dataType' => 2, 'precision' => 50, 'description' => 302442, 'required' => 1);
        $customTemplate[] = array('column' => 'nmfield04', 'term' => 302443, 'dataType' => 2, 'precision' => 50, 'description' => 302443, 'required' => 1,  'note'=>'intershot');

        $sxml = new  \SimpleXMLElement(('<?xml version="1.0" encoding="utf-8"?><file></file>'));
        $sxml->addAttribute('filename', $Nmfile);

        $operation = $sxml->addChild('operations');
        $operation->addAttribute('opname', $Nmoperation);

        foreach ($customTemplate as $key => $item) {
            $serverKeys = array_keys($item);
            $import = $operation->addChild('import');
            $import->addAttribute('import_id', $Nmimport);

            foreach ($serverKeys as $i => $name) {
                $ops = $import->addChild($name);
                $ops->addAttribute($name, $item[$name]);
           }
        }

        var_dump($sxml->asXML());


    }
}