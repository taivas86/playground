<?php


namespace App\Controller\CodingBat\WarmUp1;

// Given a string, take the last char and return a new string with the last char added at the front and back, so "cat" yields "tcatt".
// The original string will be length 1 or more.
// backAround("cat") → "tcatt"
// backAround("Hello") → "oHelloo"
// backAround("a") → "aaa"


class BackAround
{
    public static function solveBackAround($string) {
        $lastChar = $string[strlen($string)-1];
        return $lastChar.$string.$lastChar;
    }
}