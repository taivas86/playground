<?php

namespace App\Controller\CodingBat\Array2;

//Return the sum of the numbers in the array, returning 0 for an empty array.
//Except the number 13 is very unlucky, so it does not count and numbers that come
//immediately after a 13 also do not count.
//sum13([1, 2, 2, 1]) → 6
//sum13([1, 1]) → 2
//sum13([1, 2, 2, 1, 13]) → 6

class Sum13
{
    public static function solveSum13($arr): int
    {

        if (empty($arr)) {
            return 0;
        }

        $sum = 0;

        for ($i = 0; $i < count($arr); $i++) {
                if ($arr[$i] != 13) {
                    $sum += $arr[$i];
                } else {
                    break;
                }
        }

        return $sum;
    }
}