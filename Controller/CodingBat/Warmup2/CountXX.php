<?php


namespace App\Controller\CodingBat\Warmup2;

//Count the number of "xx" in the given string. We'll say that overlapping is allowed, so "xxx" contains 2 "xx".
//countXX("abcxx") → 1
//countXX("xxx") → 2
//countXX("xxxx") → 3

class CountXX
{
    public static function solveCountXX($string) :int {

        $count = 0;

        for ($i=0; $i<strlen($string)-1; $i++) {
            if ($string[$i] == "x" && $string[$i+1]=="x") {
                $count++;
            }
        }


        return $count;
    }
}