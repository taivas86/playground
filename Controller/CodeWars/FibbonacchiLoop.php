<?php

namespace App\Controller\CodeWars;

class FibbonacchiLoop
{
    public static function solveFibonacciLoop(array $sign, int $n)
    {
        $res = [];
        for ($i=0; $i<$n; $i++) {
            if ($i<2) {
                $res[] = $sign[$i];
            } else {
                $res[] = $res[$i-1]+$res[$i-2];
            }
        }

        dump($res);
    }
}