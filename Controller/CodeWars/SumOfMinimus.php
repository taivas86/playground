<?php

namespace App\Controller\CodeWars;

class SumOfMinimus
{
    public static function solveSumOfMinimus(array $numbers): int
    {

        $len = count($numbers);
        $sum = 0;

        for ($i = 0; $i < $len; $i++) {
            $sum+= min($numbers[$i]);
        }

        return $sum;
    }
}