<?php

namespace App\Controller\CodingBat\WarmUp1;

class loneTeen
{
    //We'll say that a number is "teen" if it is in the range 13..19 inclusive.
    // Given 2 int values, return true if one or the other is teen, but not both.
    //loneTeen(13, 99) → true
    //loneTeen(21, 19) → true
    //loneTeen(13, 13) → false

    public static function solveLoneTeen($num1, $num2)
    {
        if ($num1 == $num2) {
            return false;
        }

        if (($num1 >= 13 && $num1 <= 19) | ($num2 >= 13 && $num2 <= 19)) {
            return true;
        }

        return false;
    }

}