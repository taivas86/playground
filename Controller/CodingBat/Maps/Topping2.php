<?php

namespace App\Controller\CodingBat\Maps;

//topping2({"spinach": "dirt", "ice cream": "cherry"}) → {"yogurt": "cherry", "spinach": "nuts", "ice cream": "cherry"}

class Topping2
{
    public static function solveTopping2(array $maps)
    {
//        $keys = array_keys($maps);
//        $klen = count($keys);
//
//        for ($i = 0; $i < $klen; $i++) {
//            $key = $keys[$i];
//
//            if ($key == 'spinach') {
//                $maps[$key] = 'nuts';
//            }
//
//            if ($key == 'ice cream') {
//                $maps['yogurt'] = $maps[$key];
//            }
//        }
//
//        return $maps;

        //no loop solution
        $iceberry = 'ice cream';
        if (array_key_exists($iceberry, $maps)) {
            $maps['yogurt'] = $maps[$iceberry];
        }

        $weed = 'spinach';
        if (array_key_exists($weed, $maps)) {
            $maps[$weed] = "nuts";
        }

        dump($maps);


    }
}