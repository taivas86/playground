<?php

namespace App\Controller\TRP;

class Employee
{
    private $name;
    private $salary;

    public function __construct($name, $salary) {
        $this->name = $name;
        $this->salary = $salary;
    }

    public function getName() :string
    {
        return $this->name;
    }

    public function getSalary() :float {
        return $this->salary;
    }
}