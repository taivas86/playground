<?php

namespace App\Controller\CodingBat\Maps;

class WordLen
{
    public static function solveWordLen($map)
    {
        $tmp = [];
        foreach ($map as $value) {
            //?? null coalscing (PHP7+)
            $tmp[$value] ??= 0;
            $tmp[$value] +=1;
        }

        dump($tmp);

        $keys = array_keys($tmp);
        $klen = count($keys);
        for ($i=0; $i<$klen; $i++) {
            $key = $keys[$i];
            $tmp[$key] = strlen($key);
        }

        dump($tmp);
    }
}
