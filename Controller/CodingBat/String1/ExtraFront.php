<?php

namespace App\Controller\CodingBat\String1;

//Given a string, return a new string made of 3 copies of the first 2 chars of the original string. The string may be any length. If there are fewer than 2 chars, use whatever is there.
//extraFront("Hello") → "HeHeHe"
//extraFront("ab") → "ababab"
//extraFront("H") → "HHH"

class ExtraFront
{

    public static function solveExtraFront($string): string
    {
        $buff = "";
        $copys = 3;
        $length = strlen($string);

        for ($i = 0; $i < $copys; $i++) {
            $buff.= substr($string, 0, 2);
        }
        return $buff;
    }
}
