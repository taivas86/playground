<?php


namespace App\Controller\CodingBat\Warmup2;

//Given an array of ints, return the number of 9's in the array.
//arrayCount9([1, 2, 9]) → 1
//arrayCount9([1, 9, 9]) → 2
//arrayCount9([1, 9, 9, 3, 9]) → 3


class ArrayCount9
{

    public static function solveArrayCount9($arr) : int {

        $count = 0;

        for ($i=0; $i<count($arr);$i++) {



            if ($arr[$i]==9) {
                $count++;
            }

        }

        return $count;
    }

 }