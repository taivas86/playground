<?php


namespace App\Controller\CodingBat\WarmUp1;

//Given 2 positive int values, return the larger value that is in the range 10..20 inclusive,
// or return 0 if neither is in that range.
//max1020(11, 19) → 19
//max1020(19, 11) → 19
//max1020(11, 9) → 11


class Max1020
{
    public static function solveMax1020($a, $b)
    {
        if ($a >= 10 && $b <= 20) {
            if ($a>$b) {
                return $a;
            } else
                return $b;
        }

        return 0;
    }
}