<?php


namespace App\Controller\CodingBat\Warmup2;


//Suppose the string "yak" is unlucky. Given a string, return a version where all the "yak" are removed,
//but the "a" can be any char. The "yak" strings will not overlap.
//stringYak("yakpak") → "pak"
//stringYak("pakyak") → "pak"
//stringYak("yak123ya") → "123ya"


class StringYak
{
    public static function solveStringYak($string) :string {

        $result = '';

        for ($i=0; $i<strlen($string);$i++) {
            if ($i+2<strlen($string) && $string[$i]=='y' && $string[$i+2] == 'k') {
                $i=$i+2;
            } else {
               $result.=$string[$i];
            }
        }

        return $result;
    }
}