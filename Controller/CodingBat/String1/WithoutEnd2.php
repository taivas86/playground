<?php


namespace App\Controller\CodingBat\String1;


//Given a string, return a version without both the first and last char of the string.
//The string may be any length, including 0.
//withouEnd2("Hello") → "ell"
//withouEnd2("abc") → "b"
//withouEnd2("ab") → ""

class WithoutEnd2
{
    public static function solveWithoutEnd2($string) {
        return substr(substr($string, 1),0, -1);

    }
}