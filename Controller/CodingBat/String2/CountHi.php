<?php

namespace App\Controller\CodingBat\String2;


//Return the number of times that the string "hi" appears anywhere in the given string.
//countHi("abc hi ho") → 1
//countHi("ABChi hi") → 2
//countHi("hihi") → 2


class CountHi {

    public static function solveCountHi($string) :int {

        $count = 0;

        for ($i=0; $i<strlen($string)-1; $i++) {
            if ($string[$i].$string[$i+1] == "catcat") {
               $count++;
            }
        }

        //factory methods
        dump(substr_count($string, "hi"));

        return $count;
    }

}