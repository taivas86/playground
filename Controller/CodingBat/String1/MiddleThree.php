<?php
namespace App\Controller\CodingBat\String1;


//Given a string of odd length, return the string length 3 from its middle,
//so "Candy" yields "and".
//The string length will be at least 3.
//middleThree("Candy") → "and"
//middleThree("and") → "and"
//middleThree("solving") → "lvi"

class MiddleThree {

    public static function solveMiddleThree($string) :string {

        $arr = str_split($string);

        return ($arr[count($arr)/2-1]).($arr[count($arr)/2]).($arr[count($arr)/2+1]);




    }

}
