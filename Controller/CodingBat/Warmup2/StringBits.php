<?php


namespace App\Controller\CodingBat\Warmup2;

//Given a string, return a new string made of every other char starting with the first,
//so "Hello" yields "Hlo".
//stringBits("Hello") → "Hlo"
//stringBits("Hi") → "H"
//stringBits("Heeololeo") → "Hello"

class StringBits
{
    public static function solveStringBits($str): string
    {

        $buff = "";

        dump($str);

        for ($i=0; $i < strlen($str); $i++) {

            if ($i % 2 == 0) {
                $buff.=$str[$i];
            }
        }
        return $buff;

    }
}