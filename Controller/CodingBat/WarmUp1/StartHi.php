<?php


namespace App\Controller\CodingBat\WarmUp1;


//Given a string, return true if the string starts with "hi" and false otherwise.
//startHi("hi there") → true
//startHi("hi") → true
//startHi("hello hi") → false

class StartHi
{

    public static function solveStartHi($string) {
        if (substr($string, 0,3)=="hi ") {
            return true;
        }

        return false;
    }
}