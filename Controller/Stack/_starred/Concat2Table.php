<?php

namespace App\Controller\Stack\_starred;

class Concat2Table
{
    public static function solveConcat2Table()
    {
        $rw = array_map('str_getcsv', file('/home/tosaithe/Desktop/oopbook/src/Controller/concat.csv'));

        $csv = [];
        foreach ($rw as $key => $value) {
            $csv[$key] = array_combine($rw[0], $value);
        }
        $data = array_slice($csv, 1);

        echo('<table border="1"><tbody>');

//header
        echo('<tr>');
        foreach ($data[0] as $key => $value) {
            echo("<th>{$key}</th>");
        }
        echo('</tr>');

//data
        $len = count($data);
        for ($i = 0; $i < $len; $i++) {
            $array = $data[$i];
            echo("<tr>");
            foreach ($array as $a => $item) {
                echo("<td>{$item}</td>");
            }
            echo("</tr>");
        }

        echo('</tbody></table>');
    }
}