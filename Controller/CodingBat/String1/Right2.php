<?php

namespace App\Controller\CodingBat\String1;

class Right2
{
    public static function solveRight2($string) {

        if (strlen($string) <=2) {
            return $string;
        }

        return substr($string, -2).substr($string, 0, strlen($string)-2);

    }
}