<?php

namespace App\Controller\CodingBat\WarmUp1;

//Given a string, if the string "del" appears starting at index 1, return a string where that "del" has been deleted. Otherwise, return the string unchanged.
//delDel("adelbc") → "abc"
//delDel("adelHello") → "aHello"
//delDel("adedbc") → "adedbc"

class DelDel
{
    public static function solveDelDel($string) {

        if (substr($string, 1,3) == "del") {
            return str_replace("del","",$string);
        };

    }
}