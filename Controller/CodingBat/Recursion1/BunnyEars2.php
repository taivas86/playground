<?php


namespace App\Controller\CodingBat\Recursion1;


class BunnyEars2
{
    public static function solveBunnyEars2($bunnies)
    {

        $count = 0;

        if ($bunnies == 0) {
            return 0;
        }

        if ($bunnies % 2 == 1) {
            return 2 + self::solveBunnyEars2($bunnies - 1);
        }

        return 3 + self::solveBunnyEars2($bunnies - 1);


    }
}