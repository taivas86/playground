<?php

namespace App\Controller\CodeWars;

class RockPaperScissors
{
    public static function rpc($p1, $p2)
    {
        $rock = 'rock';
        $paper = 'papper';
        $scissors = 'scissors';

        if (($p1 == $rock && $p2 == $scissors) ||
            ($p1 == $scissors && $p2 == $paper) ||
            ($p1 == $paper && $p2 == $rock)) {
            return 'Player 1 won!';
        }

        if ($p1 == $p2) {
            return 'Draw';
        }

        return 'Player 2 won!';
    }
}