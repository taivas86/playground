<?php


namespace App\Controller\CodingBat\String1;


class firstHall
{
    public static function solveFirstHall($string) :string {

        $half = "";

        for ($i=0; $i<strlen($string)/2; $i++) {
            $half.=$string[$i];
        }

        return $half;
    }
}