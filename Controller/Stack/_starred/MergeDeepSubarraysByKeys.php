<?php

namespace App\Controller\Stack\_starred;

//https://stackoverflow.com/questions/52184042/how-to-merge-subarrays-by-one-key-value-and-push-qualifying-values-into-a-deeper

class MergeDeepSubarraysByKeys
{
    public static function solveMergeSubarrays()
    {
        $input = [
            [
                'hash' => '948e980ed2a36d917f1b3026b04e0016',
                'filename' => ['73315_73316_73317_LAKE13437_322.tif'],
                'filepath' => ['_TEST_DATA']
            ],
            [
                'hash' => '948e980ed2a36d917f1b3026b04e0016',
                'filename' => ['73315_73316_73317_LAKE13437_322(1).tif'],
                'filepath' => ['_TEST_DATA/subdirectory']
            ]
        ];

        $output = [];
        foreach ($input as $value) {
            $output[$value['hash']] ['hash']=$value['hash'];
            $output[$value['hash']] ['filename'][] = $value['filename'][0];
            $output[$value['hash']] ['filepath'][] = $value['filepath'][0];
        }

        return array_values($output);
    }
}