<?php

namespace App\Controller\CodingBat\String1;

//Given a string of even length, return a string made of the middle two chars, so the string "string" yields "ri".
// The string length will be at least 2.
//middleTwo("string") → "ri"
//middleTwo("code") → "od"
//middleTwo("Practice") → "ct"

class MiddleTwo
{
    public static function solveMiddleTwo($str)
    {
        $num = strlen($str)/2;
        return $str[$num-1].$str[$num];
    }
}

