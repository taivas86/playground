<?php

namespace App\Controller\CodeWars;

class ReplaceAllVowels
{
    public static function solveReplaceAll(string $s): string {
        $vowels = 'aeiouAEIOU';
        $vowelsArr = str_split($vowels,1);
        return str_replace($vowelsArr, '!', $s);
    }
}