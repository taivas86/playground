<?php


namespace App\Controller\CodingBat\Recursion1;


class BunnyEars
{
    public static function solveBunnyEars($bunnies)
    {

        if ($bunnies == 0) {
            return 0;
        } else {
            return self::solveBunnyEars($bunnies - 1) + 2;
        }


    }
}