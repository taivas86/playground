<?php


namespace App\Controller\CodingBat\Warmup2;


//Given an array of ints, return true if the sequence of numbers 1, 2, 3 appears in the array somewhere.
//array123([1, 1, 2, 3, 1]) → true
//array123([1, 1, 2, 4, 1]) → false
//array123([1, 1, 2, 1, 2, 3]) → true


class Array123
{
    public static function solveArray123($arr) :bool {

        for ($i=0; $i<count($arr)-2; $i++) {

           if ($arr[$i] == 1 && $arr[$i+1]==2 && $arr[$i+2]==3) {
               return true;
           }

            dump($arr[$i], $arr[$i+1], $arr[$i+2]);
        }

        return false;
    }
}