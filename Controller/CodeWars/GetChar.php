<?php

namespace App\Controller\CodeWars;

class GetChar
{
    public static function solveGetChar($c) {
        return chr($c);
    }
}