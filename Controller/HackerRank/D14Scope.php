<?php

namespace App\Controller\HackerRank;

class D14Scope
{
    public static function solveD14($arr)
    {
        $len = count($arr);
        //outbox
        //return abs(max($arr)-min($arr));

        //manual proctology
        for ($i = 0; $i < $len; $i++) {
            for ($j = $len - 1; $j > $i; $j--) {
                if ($arr[$j]<$arr[$j-1]) {
                    $tmp = $arr[$j];
                    $arr[$j] = $arr[$j-1];
                    $arr[$j-1] = $tmp;
                }
            }
        }
        return abs($arr[$len-1]-$arr[0]);
    }
}