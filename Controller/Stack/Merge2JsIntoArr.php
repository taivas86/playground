<?php

namespace App\Controller\Stack;
//https://stackoverflow.com/questions/75370909/how-to-merge-two-json-without-using-of-array-merge-or-array-combine-in-php
class Merge2JsIntoArr
{
    public static function solveMergeTwoArr()
    {
        $a = '[{ "id": "8", "first_name": "Tess"}]';
        $b = '[{"id": "8",  "last_name": "Doe"}]';

        $ajs = json_decode($a);
        $bjs = json_decode($b);

        function prep($ar, $br)  {
            $tmp = [];
            $arLen = count($ar);
            $brLen = count($br);

            //$a
            for ($i=0; $i<$arLen; $i++) {
                $tmp[] = ['id'=>$ar[$i]->id, 'first_name' => $ar[$i]->first_name];
            }

            //$b
            for ($j=0; $j<$brLen; $j++) {
                $tmp[] = ['id'=>$br[$j]->id, 'last_name' => $br[$j]->last_name];
            }
            return $tmp;
        }

        $atmp = prep($ajs, $bjs);

        foreach ($atmp as $value) {
            $output[$value['id']] ['id']=$value['id'];
            $output[$value['id']] ['first_name']=$atmp[0]['first_name'];
            $output[$value['id']] ['last_name']=$atmp[1]['last_name'];
        }
        dump(current($output));
    }
}
