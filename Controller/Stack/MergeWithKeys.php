<?php

namespace App\Controller\Stack;

//https://stackoverflow.com/questions/75147502/merge-arrays-in-laravel-including-keys

class MergeWithKeys
{

    /**
     * [
     * "id" => "112802",
     * "price" => "500000",
     * "note" => "note 2",
     * ],
     * [
     * "id" => "113041",
     * "price" => "1000000",
     * "note" => "note 1",
     *
     * ]
     * }]
     */


    public static function solveMergeKeys($prices, $notes)
    {
        $pricesBuff = [];
        $len = count($prices);
        for ($i=0; $i<$len; $i++) {
            $element = $prices[$i];
            $pricesBuff[] = ['id'=>key($element), 'price'=>current($element)];
        }

        $notesBuff = [];
        $lenNotes = count($notes);
        for ($i=0; $i<$lenNotes; $i++) {
            $element = $notes[$i];
            $notesBuff[] = ['id'=>key($element), 'note'=>current($element)];
        }


     return array_replace_recursive($pricesBuff, $notesBuff);
    }
}
