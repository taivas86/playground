<?php

namespace App\Controller\CodingBat\Array2;

//Given an array of ints, return true if it contains no 1's or it contains no 4's.
//no14([1, 2, 3]) → true
//no14([1, 2, 3, 4]) → false
//no14([2, 3, 4]) → true


class No14 {

    public static function solveNo14($arr) :bool {

        for ($i=0; $i<count($arr); $i++) {
            if($arr[$i] == 1 || $arr[$i] == 4) {
                return false;

            }
        }

        return true;

    }

}
