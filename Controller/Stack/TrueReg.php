<?php

namespace App\Controller\Stack;

//https://stackoverflow.com/questions/75151097/how-to-return-true-with-time


class TrueReg
{
    public static function solveTrueReg($udata) {
        $regDate = strtotime($udata['joined']);
        $now = strtotime('now');
        $period = $now-$regDate;
        $days = floor($period/86400);
        return $days>=30;

    }
}