<?php


namespace App\Controller\CodingBat\Warmup2;

//Given a string, return true if the first instance of "x" in the string is immediately
//followed by another "x".
//doubleX("axxbb") → true
//doubleX("axaxax") → false
//doubleX("xxxxx") → true

class DoubleX
{
    public static function solveDoubleX($string) :bool {

       for ($i=0; $i<strlen($string)-1; $i++) {
           $state = ($string[$i]==$string[$i+1]);
       }

       return $state;
    }
}