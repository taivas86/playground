<?php

namespace App\Controller\Stack;
//https://stackoverflow.com/questions/75227869/how-can-i-sort-nested-array-by-alphabetical-order-with-php

class SortNestedArrAlphabet
{
    public static function solveSortNestedAlphabet()
    {

        $rw = file_get_contents('/home/tosaithe/Desktop/oopbook/public/files/SortNestedArrAlphabet.json');
        $data = json_decode($rw, true);

        arsort($data);
    }
}