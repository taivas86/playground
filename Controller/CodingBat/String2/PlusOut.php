<?php

namespace App\Controller\CodingBat\String2;

class PlusOut
{
    public static function solvePlusOut(string $string, string $word)
    {
        $len = strlen($string);
        $pos = strpos($string, $word);
        $wlen = strlen($word);
        $buff = '';
        for ($i = 0; $i < $len-1; $i++) {
            $chunck = $string[$i];

            if ($i == $pos) {
                $j = $pos;
                while ($j < $pos + $wlen) {
                    $buff .=$string[$j];
                    $j++;
                }
            }
            $buff .= "+";
        }

        return $buff;
    }
}
