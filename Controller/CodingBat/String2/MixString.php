<?php

namespace App\Controller\CodingBat\String2;

//Given two strings, a and b, create a bigger string made of the first char of a, the first char of b, the second char of a, the second char of b, and so on.
//Any leftover chars go at the end of the result.
//mixString("abc", "xyz") → "axbycz"
//mixString("Hi", "There") → "HTihere"
//mixString("xxxx", "There") → "xTxhxexre"


class MixString
{

    public static function solveMixString($strA, $strB): string
    {

        $buff = "";

        for ($i = 0; $i < strlen($strA); $i++) {
            for ($j = 0; $j < strlen($strB); $j++) {
                $buff.=$strA[$i].$strB[$j];
            }
        }

        return $buff;
    }
}