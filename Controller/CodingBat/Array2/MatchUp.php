<?php

namespace App\Controller\CodingBat\Array2;

//Given arrays nums1 and nums2 of the same length, for every element in nums1, consider the corresponding element
// in nums2 (at the same index).
// Return the count of the number of times that the two elements differ by 2 or less, but are not equal.
//matchUp([1, 2, 3], [2, 3, 10]) → 2
//matchUp([1, 2, 3], [2, 3, 5]) → 3
//matchUp([1, 2, 3], [2, 3, 3]) → 2


class MatchUp
{

    public static function solveMatchUp($arr1, $arr2): int
    {

        $count = 0;

        for ($i = 0; $i < count($arr1); $i++) {
            if (abs($arr1[$i] - $arr2[$i]) == 1 ||
                abs($arr1[$i] - $arr2[$i]) == 2)
                $count++;
        }

        return $count;

    }

}