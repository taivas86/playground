<?php

namespace App\Controller\TRP;

class Cart
{
    private $products = [];

    public function add($product) :void
    {
        $this->products[] = $product;
    }

    public function remove($item)
    {
        $prArr = $this->products;
        $len = count($prArr);
        for ($i = 0; $i < $len; $i++) {
            if ($prArr[$i]->getName() == $item) {
                unset($prArr[$i]);
            }
        }
        return $prArr;
    }

    public function getTotalCost() :float
    {
        $prArr = $this->products;
        $len = count($prArr);
        $sum = 0;
        for ($i = 0; $i < $len; $i++) {
            $sum += $prArr[$i]->getPrice();
        }

        return $sum;
    }

    public function getTotalQnt() :int|float {
        $prArr = $this->products;
        $len = count($prArr);
        $qnt = 0;
        for ($i=0; $i<$len; $i++) {
            $qnt += $prArr[$i]->getQuantity();
        }

        return $qnt;
    }

    public function getAvg() :float {
        return $this->getTotalCost()/$this->getTotalQnt();
    }



}