<?php

namespace App\Controller\TRP;

class AvgHelper
{
   public function getAvg($arr) {
       return array_sum($arr)/count($arr);
   }

   public function getMeanSquare($arr) {
       $sum = 0;

       foreach ($arr as $value) {
           $sum += pow($value, 2);
       }

       return pow($sum, 1/2);
   }
}