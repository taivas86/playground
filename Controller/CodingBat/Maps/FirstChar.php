<?php

namespace App\Controller\CodingBat\Maps;

class FirstChar
{
    public static function solveFirstChar($array)
    {
        $keysArr = [];
        foreach ($array as $value) {
            $key = substr($value, 0, 1);
            $keysArr[] = $key;
        }
        $keysArr = array_unique($keysArr);
        sort($array);

        $valueArr = [];
        $len = count($array);
        for ($i = 0; $i < $len - 1; $i++) {
            $elt = $array[$i];
            $eltNext = $array[$i + 1];
            if (substr($elt, 0, 1) == substr($eltNext, 0, 1)) {
                $valueArr[] .= $elt . $eltNext;
            }
        }

        $res = array_combine($keysArr, $valueArr);
        dump($res);
    }


}
