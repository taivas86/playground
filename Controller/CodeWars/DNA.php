<?php

namespace App\Controller\CodeWars;

class DNA
{
    public static function DNA_strand(string $dna): string
    {

        $len = strlen($dna);
        for ($i = 0; $i < $len; $i++) {
            $element = $dna[$i];
            if ($element == 'A') $dna[$i] = 'T';
            if ($element == 'T') $dna[$i] = 'A';
            if ($element == 'G') $dna[$i] = 'C';
            if ($element == 'C') $dna[$i] = 'G';
        }

        return $dna;
    }
}