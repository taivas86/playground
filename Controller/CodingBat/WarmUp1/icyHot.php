<?php


namespace App\Controller\CodingBat\WarmUp1;

//Given two temperatures, return true if one is less than 0 and the other is greater than 100.
//icyHot(120, -1) → true
//icyHot(-1, 120) → true
//icyHot(2, 120) → false


class icyHot
{

    public static function solveIcyHot($temp1, $temp2){

        if (($temp1 < 0 && $temp2 > 0)||($temp1 > 0 && $temp2 < 0)) {
            return true;
        }

        return false;

    }

}