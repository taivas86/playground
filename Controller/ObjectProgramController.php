<?php

namespace App\Controller;

use App\Controller\Stack\DeutchlandFootbal;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ObjectProgramController extends AbstractController
{
    /**
     * @Route("123", name="object_program")
     */
    public function index(): Response
    {
        $result = DeutchlandFootbal::solveDeutchFootball();
        return self::json(['message' => $result]);
    }
}
