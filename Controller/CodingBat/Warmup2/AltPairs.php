<?php


namespace App\Controller\CodingBat\Warmup2;

//Given a string, return a string made of the chars at indexes 0,1, 4,5, 8,9 ...
//so "kittens" yields "kien".
//altPairs("kitten") → "kien"
//altPairs("Chocolate") → "Chole"
//altPairs("CodingHorror") → "Congrr"



class AltPairs
{
    public static function solveAltPairs($str) :string {

        $buff = "";

        for ($i=0; $i<strlen($str)-1; $i=$i+4) {
            $buff.= $str[$i].$str[$i+1];
        }


        return $buff;
    }
}