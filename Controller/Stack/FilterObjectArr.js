const data = [{
    "uniqueId": 1233, "serviceTags": [{
        "Id": 11602, "tagId": "FRRRR", "missingRequired": [], "summaries": [{
            "contract": "ACTIVE",

        }, {
            "contract": "INACTIVE",

        }], "lttributes": {}
    }],
}, {
    "uniqueId": 34555, "serviceTags": [{
        "Id": 11602, "tagId": "JWJN2", "missingRequired": [], "summaries": [{
            "contract": "ACTIVE",

        }, {
            "contract": "INACTIVE",

        }], "lttributes": {}
    }],
}, {
    "uniqueId": 44422, "serviceTags": [{
        "Id": 11602, "tagId": "ABC", "missingRequired": [], "summaries": [{
            "contract": "ACTIVE",

        }, {
            "contract": "INACTIVE",

        }], "lttributes": {}
    }, {
        "Id": 11602, "tagId": "BBC", "missingRequired": [], "summaries": [{
            "contract": "ACTIVE",

        }, {
            "contract": "INACTIVE",

        }], "lttributes": {}
    }],
}]

const tagId = ['ABC', 'FRRRR'];

len = data.length;
arr = [];
data.forEach(function (element,) {
    let tagElement = element.serviceTags;
    if (tagId.includes(tagElement[0].tagId)) {
        arr.push(element);
    }
})

console.log(arr);

//stack solution
r = data.map(({serviceTags, ...rest}) => ({
    rest, serviceTags: serviceTags.filter(({tagId: tid}) => tagId.includes(tid))
}))
    .filter(d => d.serviceTags.every(c => tagId.includes(c.tagId)));


console.log(r);
