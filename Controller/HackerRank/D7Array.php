<?php

namespace App\Controller\HackerRank;

class D7Array
{
    public static function solveD7($arr)
    {
        //outbox methods
        $arrRev = array_reverse($arr);
        $buff = implode(" ", $arrRev);
        //4, 3, 2, 1

        //one solution with buff;
        $tmp = [];
        $len = count($arr);

        for ($i = $len-1; $i>=0; $i--) {
            $tmp[] = $arr[$i]." ";
        }
        $buff1 = implode('',$tmp);
        dump($buff1);

        //two solution without buff
        for ($i=0; $i<$len/2;$i++){
            $temp = $arr[$i];
            $arr[$i]=$arr[$len-$i-1];
            $arr[$len-$i-1] = $temp;
        }

        return $arr;


    }


}