<?php


namespace App\Controller\CodingBat\Array2;

//Given an array of ints, return true if the array contains either 3 even or 3 odd values all next to each other.
//modThree([2, 1, 3, 5]) → true
//modThree([2, 1, 2, 5]) → false
//modThree([2, 4, 2, 5]) → true


class ModThree
{
    public static function solveModThree($arr)
    {

        $result = false;

        for ($i = 0; $i < count($arr) - 2; $i++) {
            if (($arr[$i] % 2 == 0 && $arr[$i + 1] % 2 == 0 && $arr[$i + 2] % 2 == 0) ||
                (!($arr[$i] % 2 == 0) && !($arr[$i + 1] % 2 == 0) && !($arr[$i + 2] % 2 == 0)))

                $result = true;
        }

        return $result;
    }

}