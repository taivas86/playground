<?php

namespace App\Controller\HackerRank;

class D6Review
{
    public static function solveD6($string)
    {
        $len = strlen($string);
        $buff = '';
        for ($i = 0; $i <= $len - 1; $i++) {
            if ($i % 2 == 0) {
                $buff .= $string[$i];
            }
        }
        $buff = $buff." ";

        for ($i = 0; $i <= $len - 1; $i++) {
            if ($i % 2 != 0) {
                $buff .= $string[$i];
            }
        }


        return $buff;
    }
}