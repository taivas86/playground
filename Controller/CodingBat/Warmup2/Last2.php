<?php


namespace App\Controller\CodingBat\Warmup2;

//Given a string, return the count of the number of times that a substring
//length 2 appears in the string and also as the last 2 chars of the string,
//so "hixxxhi" yields 1 (we won't count the end substring).
//last2("hixxhi") → 1
//last2("xaxxaxaxx") → 1
//last2("axxxaaxx") → 2


class Last2
{
    public static function solveLast2($str) :int {

        //если длина строки <2 возвращаем ноль
        if (strlen($str) <2 ) {
            return 0;
        }

        //берем 2 последних чара в кусок
        $endOfString = substr($str, strlen($str)-2);

        $count = 0;

        for ($i=0; $i<strlen($str)-1; $i++) {

            $sub = substr($str, $i, $i+1);

            dump($sub);

            if ($sub == $endOfString) {
                $count++;
            }
        }

        return $count;
    }
}