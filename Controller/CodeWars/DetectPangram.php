<?php

namespace App\Controller\CodeWars;

class DetectPangram
{
    public static function detect_pangram($string)
    {
        $len = strlen($string);
        $string = strtolower($string);
        $buff = '';

        if ($len == 0) {
            return false;
        }

        //"preg_replace" bicycle
        $i = 0;
        while ($i < $len) {
            if (ctype_alpha($string[$i])) {
                $buff .= $string[$i];
            }
            $i++;
        }

        $alphabet = range('a', 'z');

        dump($alphabet, $buff);
        $countFalse = 0;

        foreach ($alphabet as $char) {
            if (str_contains($buff, $char)) {
                $countFalse++;
            }
        }

        return $countFalse < 1;
    }
}