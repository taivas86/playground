<?php

namespace App\Controller\Stack\_starred;

//https://stackoverflow.com/questions/75226621/sort-associative-array-by-values-based-on-another-array

class SortAssArrByValues
{
    public static function solveSortAssArrByValues()
    {
        $arr = array(array('id' => 1, 'name' => 'Test 1', 'classId' => 3), array('id' => 1, 'name' => 'Test 1', 'classId' => 15,), array('id' => 1, 'name' => 'Test 1', 'classId' => 17,),);
        $classIds = [15, 17, 3];

        $input = array_values($arr);
        $order = array_flip($classIds);
        $output = [];

        array_walk($input, function($entry, $index) use ($order, &$output) {
            $output[$order[$entry['classId']]] = $entry;
        });

        ksort($output);
        print_r($output);


    }
}