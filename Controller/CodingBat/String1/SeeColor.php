<?php

namespace App\Controller\CodingBat\String1;

/*
Given a string, if the string begins with "red" or "blue" return that color string,
otherwise return the empty string.
seeColor("redxx") → "red"
seeColor("xxred") → ""
seeColor("blueTimes") → "blue"
*/

class SeeColor {

    public static function solveSeeControl($string) :string {

        if (substr($string,0,3 )==="red") {
            return "red";
        }

        if (substr($string, 0, 4) === "blue") {
            return "blue";
        }

        return "ni4onet";

    }

}
