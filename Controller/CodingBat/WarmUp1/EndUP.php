<?php


namespace App\Controller\CodingBat\WarmUp1;

//Given a string, return a new string where the last 3 chars are now in upper case.
// If the string has less than 3 chars, uppercase whatever is there. Note that str.toUpperCase() returns the uppercase version of a string.
//endUp("Hello") → "HeLLO"
//endUp("hi there") → "hi thERE"
//endUp("hi") → "HI"


class EndUP
{
    public static function solveEndUp($string)
    {
        if (strlen($string) < 3) {
            return strtoupper($string);
        }
        $first = substr($string, 0, strlen($string) - 3);
        $last = strtoupper(substr($string, strlen($string) - 3));
        return $first . $last;

    }
}